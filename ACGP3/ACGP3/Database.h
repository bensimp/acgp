#pragma once

#include "Library.h"

class Database
{
	private:
		//Variables
		int playset;
		int p1turn;
		int p1turnoffset;
		int p2turn;
		int p2turnoffset;

		//access variables
		int index1;
		int index2;

		//array variants 
		int P1Questions0[20] = { 1,3,2,4,5,7,6,8,9,11,10,12,13,15,14,15,17,19,18,0 };
		int P1Questions1[20] = { 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19 };
		int P1Questions2[20] = { 0,19,1,18,2,17,3,16,4,15,5,14,6,13,7,12,8,11,9,10 };

		int P2Questions0[20] = { 0,19,1,18,2,17,3,16,4,15,5,14,6,13,7,12,8,11,9,10 };
		int P2Questions1[20] = { 1,3,2,4,5,7,6,8,9,11,10,12,13,15,14,15,17,19,18,0 };
		int P2Questions2[20] = { 1,3,2,4,5,7,6,8,9,11,10,12,13,15,14,15,17,19,18,0 };

		//Methods
		void Shuffle(int index); // shuffles answers


	public:
		//Variables
		// 
		//arrays 
		std::string Subjects[9] = { "Science", "Sports", "History", "Movies & Television", "Music", "Toys & Games", "Geography", "General Knowledge", "Literature" };
		std::string Questions[20];
		std::string Answers[20][4];
		std::string CorrectAnswer[20];

		//Methods
		Database();
		virtual ~Database();
		void Init(int set, std::string subject);
		int GetQuestions(int player); //show the random questions to the user 
		std::string DisplayQuestionChoice(); // pick two random questions and display them
		std::string DisplayQuestion(int index); // display chosen question to player whos answering
		std::string GetAnswers(int index); // get answers for thew question
		std::string CheckAnswer(int index, int choice); // check answer and return right answer or congrats
};

