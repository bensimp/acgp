#pragma once

#include<iostream>
#include<vector>
#include<cstdlib>
#include<math.h>
#include<fstream>
#include<sstream>
#include<map>
#include<stack>
#include<string>
#include <algorithm>
#include <WS2tcpip.h>

#pragma comment (lib, "ws2_32.lib")

using namespace std;

class Library
{

	public:
		Library();
		virtual ~Library();
};
