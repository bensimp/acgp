#include "TriviaAttack.h"
#include "Game.h"

//Set game grid size 
int TriviaAttack::gridSize = 50;
//Set game background size 
int TriviaAttack::backgroundSize = 500;
//Set background colour
//Color TriviaAttack::backgroundColour = Color(50, 50, 50, 255);

//Constructor 
TriviaAttack::TriviaAttack()
{

}

//Destructor 
TriviaAttack::~TriviaAttack()
{
}
 
void TriviaAttack::Run()
{
	//Init time
	srand(time(NULL));

	Game TriviaAttack;
	TriviaAttack.Play();
}
