#include "Game.h"
#include "Database.h"

/*Private Methods*/
void Game::PrintRules() 
{
	cout << "/* WELCOME */" << "\n";
	cout << "/* THIS IS TRIVIA ATTACK */" << "\n";
	cout << "\n";
	cout << "\n";
	cout << "/* GAME RULES */" << "\n";
	cout << "\n";
	cout << "1: The aim of the game is to defeat your opponent through superior knowledge." << "\n";
	cout << "2: To win you must either:" << "\n";
	cout << "Have the most Lives left at the end" << "\n";
	cout << "Have the most points at the end if You have the same amount of lives left as your opponent" << "\n";
	cout << "3: You will have 10 questions chosen by your opponent to answer" << "\n";
	cout << "4: You will have 4 Lives" << "\n";
	cout << "5: You lose a life for each incorrect answer but gain 10 points per correct answer" << "\n";
	cout << "\n";
}

void Game::GetGameChoice()
{
	int choice;
	cout << "/* Choose The Category You Wish To Play */" << "\n";
	cout << "0: Science" << "\n";
	cout << "1: Sports" << "\n";
	cout << "2: History" << "\n";
	cout << "3: Movies & Television" << "\n";
	cout << "4: Music" << "\n";
	cout << "5: Toys & Games" << "\n";
	cout << "6: Geography" << "\n";
	cout << "7: General Knowledge" << "\n";
	cout << "8: Literature" << "\n";
	cout << "\n";

	cout << "Select The Category by Typing The Corresponding Number: ";
	cin >> choice;
	while (!(choice == 0) || (choice == 1) || (choice == 2) || (choice == 3) || (choice == 4) || (choice == 5) || (choice == 6) || (choice == 7) || (choice == 8))
	{
		cout << "/* Please Enter A Number From 0 - 8! */" << "\n";
		cout << "Select The Category by Typing The Corresponding Number: ";
	}
	if (choice == 0) 
	{
		GameChoice = "Science";
	}
	if (choice == 1)
	{
		GameChoice = "Sports";
	}
	if (choice == 2) 
	{
		GameChoice = "History";
	}
	if (choice == 3)
	{
		GameChoice = "Movies & Television";
	}
	if (choice == 4)
	{
		GameChoice = "Music";
	}
	if (choice == 5)
	{
		GameChoice = "Toys & Games";
	}
	if (choice == 6)
	{
		GameChoice = "Geography";
	}
	if (choice == 7)
	{
		GameChoice = "General Knowledge";
	}
	if (choice == 8)
	{
		GameChoice = "Literature";
	}
}

void Game::GetGamelevel() 
{
	cout << "/* CHOOSE GAME DIFFICULTY */" << "\n";
	cout << "0: Novice" << "\n";
	cout << "1: Intermediate" << "\n";
	cout << "2: Master" << "\n";
	cout << "\n";

	cout << "Select The Difficulty By Typing The Corresponding Number: ";
	cin >> GameLevel;

	while (!(GameLevel == 0) || !(GameLevel == 1) || !(GameLevel == 2))
	{
		cout << "/* Please Enter A Number From 0 - 2! */" << "\n";
		cout << "Select The Difficulty By Typing The Corresponding Number: ";
		cin >> GameLevel;
	}
}

void Game::DisplayQuestionChoice() 
{
	int choice;
	if (turn % 2 == 0)
	{
		cout << "Player 2 Chose Which Question Player 1 Will Answer" << "\n";
		cout << "Will It Be: " << "\n";
		cout << "1:" << Qchoice1 << "\n";
		cout << "2:" << Qchoice2 << "\n";
		
		cout << "Enter Choice Here: ";
		cin >> choice;
		while (!(choice == 1) || !(choice == 2))
		{
			cout << "/* Please Enter 1 or 2! */" << "\n";
			cout << "Enter Choice Here: ";
			cin >> choice;
		}
	}
	else
	{
		cout << "Player 1 Chose Which Question Player 2 Will Answer" << "\n";
		cout << "Will It Be: " << "\n";
		cout << "1:" << Qchoice1 << "\n";
		cout << "2:" << Qchoice2 << "\n";

		cout << "Enter Choice Here: ";
		cin >> choice;

		while (!(choice == 1) || (choice == 2))
		{
			cout << "/* Please Enter 1 or 2! */" << "\n";
			cout << "Enter Choice Here: ";
			cin >> choice;
		}
	}

	if (choice == 1) 
	{
		QIndex = Qchoice1Index;
	}
	if (choice == 2)
	{
		QIndex = Qchoice2Index;
	}
}

void Game::CheckWinner() 
{
	if (P1Lives > P2Lives) 
	{
		cout << "Player 1 is the Winner!" << "\n";
	}
	else if (P1Lives < P2Lives)
	{
		cout << "Player 2 is the Winner!" << "\n";
	}
	else 
	{
		if (P1Points > P2Points) 
		{
			cout << "Player 1 is the Winner!" << "\n";
		}
		else if (P1Points < P2Points)
		{
			cout << "Player 2 is the Winner!" << "\n";
		}
		else 
		{
			cout << "Wow... Its a Draw" << "\n";
		}
	}
}

/*Public Methods*/

Game::Game()
{
	// remove intialise problems 
	GameChoice = "";
	GameLevel = 0;

	Qchoice1 = "";
	Qchoice2 = "";
	Qchoice1Index = 0;
	Qchoice2Index = 0;
	QIndex = 0;

	A1 = "";
	A2 = "";
	A3 = "";
	A4 = "";

	turn = 1;
	P1Lives = 4;
	P2Lives = 4;
	P1Points = 0;
	P2Points = 0;
}


Game::~Game()
{
}

void Game::Play()
{
	PrintRules();
	GetGameChoice();
	GetGamelevel();
	Database data;
	data.Init(GameLevel, GameChoice);

	//reset gameplay variables
	turn = 1;
	P1Lives = 4;
	P2Lives = 4;
	P1Points = 0;
	P2Points = 0;

	int player;
	int choice;
	std::string check;

	while (turn != 21)
	{
		if (turn % 2 == 0) 
		{
			player = 1;
		}
		else 
		{
			player = 2;
		}
		Qchoice1Index, Qchoice2Index = data.GetQuestions(player);
		Qchoice1, Qchoice2 = data.DisplayQuestionChoice();
		DisplayQuestionChoice();
		cout << "Player " << player << "Answer the Following" << "\n";
		cout << data.DisplayQuestion(QIndex);
		A1, A2, A3, A4 = data.GetAnswers(QIndex);
		cout << "0:" << A1 << "\n";
		cout << "1:" << A2 << "\n";
		cout << "2:" << A3 << "\n";
		cout << "3:" << A4 << "\n";
		cout << "\n";
		
		cout << "Enter Your Answer Here:";
		cin >> choice;

		while (!(choice == 0) || !(choice == 1) || !(choice == 2) || !(choice == 3))
		{
			cout << "Enter Your An Answer From 0 - 3 Here:";
		}

		cout << data.CheckAnswer(QIndex, choice);
		cin >> check;

		if (check != "Well Done! You Have gained 10 points!!") 
		{
			if (turn % 2 == 0)
			{
				P1Lives -= 1;
			}
			else
			{
				P2Lives -= 1;
			}
		}
		else 
		{
			if (turn % 2 == 0)
			{
				P1Points += 10;
			}
			else
			{
				P2Points += 10;
			}
		}
		turn += 1;
	}
	CheckWinner();
}


