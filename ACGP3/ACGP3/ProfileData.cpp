#include "ProfileData.h"

ProfileData::ProfileData()
{
	highestScore = 0;
	totalScore = 0;

}

ProfileData::~ProfileData()
{
}


void ProfileData::createAccount(string username, string password)
{
	string userAndPass = username + " " + password;
	ofstream registerLogin;
	registerLogin.open("LoginData.txt", ios::app);

	if (registerLogin.is_open())
	{
		registerLogin << userAndPass << endl;

		registerLogin.close();
	}

	string userData = username + " " + "0" + " " + "0";
	ofstream registerData;
	registerData.open("ProfileData.txt", ios::app);

	if (registerData.is_open())
	{
		registerData << userData << endl;

		registerData.close();
	}

	string score = "0";
	string leaderboardData = score + " " + username;
	ofstream registerRank;
	registerRank.open("Leaderboard.txt", ios::app);

	if (registerRank.is_open())
	{
		registerRank << leaderboardData << endl;

		registerRank.close();
	}
}

bool ProfileData::checkLogin(string username, string password)
{
	string userAndPass = username + " " + password;
	string line;
	ifstream loginData;
	loginData.open("LoginData.txt");
	
	if (loginData.is_open())
	{	
		while (getline(loginData, line))
		{		
			if (line.find(userAndPass, 0) != string::npos)
			{
				return true;
			}
		}
		loginData.close();
	}
	return false;
}

void ProfileData::display(string username)
{
	if (username != "Guest") 
	{
		cout << "Username: " << username << endl;

		string userData = username;
		string word = "";
		int iteration = 0;;
		string line;
		ifstream profileData;
		profileData.open("ProfileData.txt");

		if (profileData.is_open())
		{
			while (getline(profileData, line))
			{	
				istringstream iss(line);
				iss >> username;
				if (username == userData)
				{
					istringstream data(line);
					data >> username >> highestScore >> totalScore;
				}
			}
			profileData.close();
		}

		cout << "Highscore: " << highestScore << endl;
		cout << "Total Score: " << totalScore << endl;
	}
	else 
	{
		cout << "Username: " << username << endl;
		cout << endl;
		cout << endl;
		cout << "Login to save your score!" << endl;
	}
}

void ProfileData::displayLeaderboard()
{
	int index = 0;
	string line;
	string ID;
	string score;
	ifstream profileRanks;
	profileRanks.open("Leaderboard.txt");

	if (profileRanks.is_open()) 
	{
		while (getline(profileRanks, line))
		{
			istringstream iss(line);
			iss >> score >> ID;
			profileScores[index][0] = score;
			profileScores[index][1] = ID;
			index++;
		}
		profileRanks.close();
	}

	int i, j;
	string temp, temp2;

	for (i = 0; i < index; i++) 
	{
		for (j = i + 1; j < index; j++) 
		{
			string score1 = profileScores[i][0];
			string score2 = profileScores[j][0];

			if (stoi(score1) < stoi(score2))
			{
				temp = profileScores[i][0];
				temp2 = profileScores[i][1];
				profileScores[i][0] = profileScores[j][0];
				profileScores[i][1] = profileScores[j][1];
				profileScores[j][0] = temp;
				profileScores[j][1] = temp2;
			}
		}
	}

	for (int k = 0; k < index; k++) 
	{
		cout << profileScores[k][0] << " " << profileScores[k][1] << endl;
	}		
}

void ProfileData::update(string username, string highestScore, string totalScore)
{
	int indx = 0;
	string row;
	string id;
	string highscore;
	string totalscore;
	ifstream profileData;
	profileData.open("ProfileData.txt");

	//place file contents into array
	if (profileData.is_open())
	{
		while (getline(profileData, row))
		{
			istringstream iss(row);
			iss >> id >> highscore >> totalscore;
			profileUpdate[indx][0] = id;
			profileUpdate[indx][1] = highscore;
			profileUpdate[indx][2] = totalscore;
			indx++;
		}
		profileData.close();
	}

	//update specific user data
	for (int k = 0; k < indx; k++)
	{
		if (username == profileUpdate[k][0])
		{
			profileUpdate[k][1] = highestScore;
			profileUpdate[k][2] = totalScore;
		}
	}

	//clear file
	ofstream file("ProfileData.txt");
	file.close();

	//re populate file
	ofstream PopulateprofileData;
	PopulateprofileData.open("ProfileData.txt", ios::app);
	string populate;

	if (PopulateprofileData.is_open())
	{
		for (int k = 0; k < indx; k++)
		{
			id = profileUpdate[k][0];
			highscore = profileUpdate[k][1];
			totalscore = profileUpdate[k][2];

			populate = id + " " + highscore + " " + totalscore;

		}
		PopulateprofileData.close();
	}	


	int index = 0;
	string line;
	string ID;
	string score;
	ifstream profileRanks;
	profileRanks.open("Leaderboard.txt");

	//place file contents into array
	if (profileRanks.is_open())
	{
		while (getline(profileRanks, line))
		{
			istringstream iss(line);
			iss >> score >> ID;
			scoresUpdate[index][0] = score;
			scoresUpdate[index][1] = ID;
			index++;
		}
		profileRanks.close();
	}

	//update specific user data
	for (int k = 0; k < index; k++)
	{
		if ( username == scoresUpdate[k][1])
		{
			scoresUpdate[k][0] = totalScore;
		}
	}

	//clear file
	ofstream File("Leaderboard.txt");
	File.close();

	//re populate file
	ofstream PopulateprofileRanks;
	PopulateprofileRanks.open("Leaderboard.txt", ios::app);
	string populater;

	if (PopulateprofileRanks.is_open())
	{
		for (int k = 0; k < indx; k++)
		{
			score = scoresUpdate[k][0];
			ID = scoresUpdate[k][1];

			populater = score + " " + ID;

			PopulateprofileRanks << populater << endl;
		}
		
		PopulateprofileRanks.close();
	}
	
}

