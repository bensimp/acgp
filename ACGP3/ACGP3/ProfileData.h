#pragma once

#include "Library.h"

class ProfileData
{
private:
	//Variables
	int highestScore;
	int totalScore;

	//arrays
	string profileScores[100][2];
	string scoresUpdate[100][2];
	string profileUpdate[100][3];

public:
	//Variables

	//Methods
	ProfileData();
	virtual ~ProfileData();
	void createAccount(string username, string password);
	bool checkLogin(string username, string password);
	void display(string username);
	void displayLeaderboard();
	void update(string username, string highestScore, string totalScore);
};
