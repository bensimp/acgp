#include "Database.h"

/*Private Methods */

void Database::Shuffle(int index)
{
	srand(time(NULL));

	int i = 3;
	int j;
	std::string temp[1];

	while (i != -1)
	{
		j = rand() % 4;
		temp[0] = Answers[index][i];
		Answers[index][i] = Answers[index][j];
		Answers[index][j] = temp[0];
		i -= 1;
	}
	
}

/*Public Methods */

Database::Database()
{
	// remove intialise problems 
	playset = 0;
	index1 = 0;
	index2 = 0;

	p1turn = 0;
	p1turnoffset = 1;
	p2turn = 0;
	p2turnoffset = 1;
}


Database::~Database()
{
}

void Database::Init(int set, std::string subject)
{
	if (subject == Subjects[0])
	{
		if (set == 0)
		{
			/*Init Science Questions*/
			Questions[0] = "What is the biggest planet in our solar system?";
			Questions[1] = "The fear of what animal is known as �arachnophobia�?";
			Questions[2] = "Pure water has a pH level of a around?";
			Questions[3] = "What does DNA stand for?";
			Questions[4] = "What essential gas is important so that we can breath?";
			Questions[5] = "What�s the boiling point of water?";
			Questions[6] = "What is the largest known animal?";
			Questions[7] = "What is a Geiger counter used to measure?";
			Questions[8] = "What is the nearest planet to the sun?";
			Questions[9] = "How many elements are there in the periodic table?";
			Questions[10] = "How many hearts does an octopus have?";
			Questions[11] = "What is the chemical formula for water?";
			Questions[12] = "Table salt is also known as what?";
			Questions[13] = "Roughly what is the gravity here on Earth?";
			Questions[14] = "The voltage in a circuit is measured using which equation?";
			Questions[15] = "What device measures electrical resistance in a circuit?";
			Questions[16] = "What organ provides oxygen to the blood?";
			Questions[17] = "What is the largest known land animal?";
			Questions[18] = "Animals that eat both plants and meat are called what?";
			Questions[19] = "What do Antibiotics treat?";


			/*Init Science Answers*/
			Answers[0][0] = "Jupiter";
			Answers[0][1] = "Earth";
			Answers[0][2] = "Saturn";
			Answers[0][3] = "Pluto";

			Answers[1][0] = "Spiders";
			Answers[1][1] = "Space";
			Answers[1][2] = "Seals";
			Answers[1][3] = "Seagulls";

			Answers[2][0] = "7";
			Answers[2][1] = "13";
			Answers[2][2] = "0";
			Answers[2][3] = "9";

			Answers[3][0] = "Deoxyribonucleic acid";
			Answers[3][1] = "Dynastic acid";
			Answers[3][2] = "Dynastic Nuclei acid";
			Answers[3][3] = "DNA";

			Answers[4][0] = "Oxygen";
			Answers[4][1] = "Air";
			Answers[4][2] = "Nitrogen";
			Answers[4][3] = "Helium";

			Answers[5][0] = "100 degrees Celsius";
			Answers[5][1] = "100 degrees Fahrenheit";
			Answers[5][2] = "100 Radians";
			Answers[5][3] = "100 degrees Kelvin";

			Answers[6][0] = "Blue Whale";
			Answers[6][1] = "Elephant";
			Answers[6][2] = "Human";
			Answers[6][3] = "Great White Shark";

			Answers[7][0] = "Radiation";
			Answers[7][1] = "Geiger";
			Answers[7][2] = "Nuclei";
			Answers[7][3] = "Bombs";

			Answers[8][0] = "Mercury";
			Answers[8][1] = "Jupiter";
			Answers[8][2] = "Earth";
			Answers[8][3] = "Titan";

			Answers[9][0] = "118";
			Answers[9][1] = "120";
			Answers[9][2] = "300";
			Answers[9][3] = "250";

			Answers[10][0] = "3";
			Answers[10][1] = "1";
			Answers[10][2] = "2";
			Answers[10][3] = "10";

			Answers[11][0] = "H2O";
			Answers[11][1] = "2H2O";
			Answers[11][2] = "2HO";
			Answers[11][3] = "HO2";

			Answers[12][0] = "Sodium Chloride";
			Answers[12][1] = "Sodium Hychloride";
			Answers[12][2] = "Sodium Chlorine";
			Answers[12][3] = "Salt";

			Answers[13][0] = "9.80 metres per second per second";
			Answers[13][1] = "9.80 metres per second";
			Answers[13][2] = "10 metres per second per second";
			Answers[13][3] = "9.08 metres per second";

			Answers[14][0] = "V = IR";
			Answers[14][1] = "V = I/R";
			Answers[14][2] = "V = R * IR";
			Answers[14][3] = "VI = R";

			Answers[15][0] = "Ohmmeter";
			Answers[15][1] = "Ohm Monitor";
			Answers[15][2] = "Ohmometer";
			Answers[15][3] = "Resistance Calculator";

			Answers[16][0] = "Lungs";
			Answers[16][1] = "Heart";
			Answers[16][2] = "Arteries";
			Answers[16][3] = "Nose";

			Answers[17][0] = "African Elephant";
			Answers[17][1] = "Indian Elephant";
			Answers[17][2] = "Yeti";
			Answers[17][3] = "Big Foot";

			Answers[18][0] = "Omnivores";
			Answers[18][1] = "Carnivores";
			Answers[18][2] = "Herbivores";
			Answers[18][3] = "Freaks";

			Answers[19][0] = "Bacterial Infections";
			Answers[19][1] = "Viral Infections";
			Answers[19][2] = "Biotics";
			Answers[19][3] = "Flu";

			/*Init Science Correct Answers*/
			CorrectAnswer[0] = Answers[0][0];
			CorrectAnswer[1] = Answers[1][0];
			CorrectAnswer[2] = Answers[2][0];
			CorrectAnswer[3] = Answers[3][0];
			CorrectAnswer[4] = Answers[4][0];
			CorrectAnswer[5] = Answers[5][0];
			CorrectAnswer[6] = Answers[6][0];
			CorrectAnswer[7] = Answers[7][0];
			CorrectAnswer[8] = Answers[8][0];
			CorrectAnswer[9] = Answers[9][0];
			CorrectAnswer[10] = Answers[10][0];
			CorrectAnswer[11] = Answers[11][0];
			CorrectAnswer[12] = Answers[12][0];
			CorrectAnswer[13] = Answers[13][0];
			CorrectAnswer[14] = Answers[14][0];
			CorrectAnswer[15] = Answers[15][0];
			CorrectAnswer[16] = Answers[16][0];
			CorrectAnswer[17] = Answers[17][0];
			CorrectAnswer[18] = Answers[18][0];
			CorrectAnswer[19] = Answers[19][0];
		}

		if (set == 1)
		{
			/*Init Science Questions*/
			Questions[0] = "How many teeth does an average adult human have?";
			Questions[1] = "What do bees collect and use to create honey?";
			Questions[2] = "What is the hottest planet in the solar system?";
			Questions[3] = "How many bones do sharks have in total?";
			Questions[4] = "Which planet in our solar system has the most moons?";
			Questions[5] = "Who was the scientist to propose the three laws of motion?";
			Questions[6] = "What two substances are the main components of bronze?";
			Questions[7] = "What is the chemical symbol for gold?";
			Questions[8] = "What is the largest living bird in the world?";
			Questions[9] = "What are the two major elements making up the sun?";
			Questions[10] = "What type of frequencies do bats use for their radar?";
			Questions[11] = "The smallest unit of data is?";
			Questions[12] = "COVID-19 is a type of?";
			Questions[13] = "What is the energy released from the sun called?";
			Questions[14] = "The time required for a quantity to reduce to half of its initial value is referred to as?";
			Questions[15] = "Which of the following is a protcol for sending emails?";
			Questions[16] = "What is the colour of the highest specturm of light?";
			Questions[17] = "In computing, what name is given to any malware which misleads users of its true intent?";
			Questions[18] = "Who is credited as the first Computer Programmer?";
			Questions[19] = "78% of the earth's atmosphere is composed of which gas?";

			/*Init Science Answers*/
			Answers[0][0] = "32";
			Answers[0][1] = "30";
			Answers[0][2] = "28";
			Answers[0][3] = "36";

			Answers[1][0] = "Nectar";
			Answers[1][1] = "Pollun";
			Answers[1][2] = "Seeds";
			Answers[1][3] = "Honey Combs";

			Answers[2][0] = "Venus";
			Answers[2][1] = "Mars";
			Answers[2][2] = "Mercury";
			Answers[2][3] = "Saturn";

			Answers[3][0] = "0";
			Answers[3][1] = "120";
			Answers[3][2] = "306";
			Answers[3][3] = "25";

			Answers[4][0] = "Saturn";
			Answers[4][1] = "Neptune";
			Answers[4][2] = "Jupiter";
			Answers[4][3] = "Mars";

			Answers[5][0] = "Isaac Newton";
			Answers[5][1] = "Ernest Rutherford";
			Answers[5][2] = "Albert Einstien";
			Answers[5][3] = "Stephen Hawking";

			Answers[6][0] = "Copper and Tin";
			Answers[6][1] = "Tin and Steel";
			Answers[6][2] = "Copper and Iron";
			Answers[6][3] = "Iron and Tin";

			Answers[7][0] = "Au";
			Answers[7][1] = "AU";
			Answers[7][2] = "au";
			Answers[7][3] = "Ua";

			Answers[8][0] = "Ostrich";
			Answers[8][1] = "Eagle";
			Answers[8][2] = "Falcon";
			Answers[8][3] = "Penguin";

			Answers[9][0] = "Hydrogen and Helium";
			Answers[9][1] = "Hydrogen and Nitrogen";
			Answers[9][2] = "Helium and Argon";
			Answers[9][3] = "Argon and Oxygen";

			Answers[10][0] = "Ultrasound";
			Answers[10][1] = "Infrasound";
			Answers[10][2] = "Hypersound";
			Answers[10][3] = "Omnisound";

			Answers[11][0] = "A Bit";
			Answers[11][1] = "A Byte";
			Answers[11][2] = "A Byt";
			Answers[11][3] = "A Bitt";

			Answers[12][0] = "Virus";
			Answers[12][1] = "Bacteria";
			Answers[12][2] = "Posion";
			Answers[12][3] = "Infection";

			Answers[13][0] = "Solar Radiation";
			Answers[13][1] = "Cosmic Radiation";
			Answers[13][2] = "Solar Energy";
			Answers[13][3] = "Solar Winds";

			Answers[14][0] = "Half Life";
			Answers[14][1] = "Lifespan";
			Answers[14][2] = "Halfspan";
			Answers[14][3] = "Reduct Time";

			Answers[15][0] = "SMTP";
			Answers[15][1] = "HTTP";
			Answers[15][2] = "TCP";
			Answers[15][3] = "ETP";

			Answers[16][0] = "Ultra Violet";
			Answers[16][1] = "Ultra Blue";
			Answers[16][2] = "Infrared";
			Answers[16][3] = "White";

			Answers[17][0] = "Trojon Horse";
			Answers[17][1] = "False Gift";
			Answers[17][2] = "Deceptive Malware";
			Answers[17][3] = "Incognito Malware";

			Answers[18][0] = "Ada Lovelace";
			Answers[18][1] = "Charles baddage";
			Answers[18][2] = "Herman Hollerith";
			Answers[18][3] = "Konrad Zuse";

			Answers[19][0] = "Nitrogen";
			Answers[19][1] = "Hydrogen";
			Answers[19][2] = "Helium";
			Answers[19][3] = "Oxygen";

			/*Init Science Correct Answers*/
			CorrectAnswer[0] = Answers[0][0];
			CorrectAnswer[1] = Answers[1][0];
			CorrectAnswer[2] = Answers[2][0];
			CorrectAnswer[3] = Answers[3][0];
			CorrectAnswer[4] = Answers[4][0];
			CorrectAnswer[5] = Answers[5][0];
			CorrectAnswer[6] = Answers[6][0];
			CorrectAnswer[7] = Answers[7][0];
			CorrectAnswer[8] = Answers[8][0];
			CorrectAnswer[9] = Answers[9][0];
			CorrectAnswer[10] = Answers[10][0];
			CorrectAnswer[11] = Answers[11][0];
			CorrectAnswer[12] = Answers[12][0];
			CorrectAnswer[13] = Answers[13][0];
			CorrectAnswer[14] = Answers[14][0];
			CorrectAnswer[15] = Answers[15][0];
			CorrectAnswer[16] = Answers[16][0];
			CorrectAnswer[17] = Answers[17][0];
			CorrectAnswer[18] = Answers[18][0];
			CorrectAnswer[19] = Answers[19][0];
		}

		if (set == 2)
		{
			/*Init Science Questions*/
			Questions[0] = "An egg�s shell is what percentage of its total weight?";
			Questions[1] = "Which part of the brain deals with hearing and language?";
			Questions[2] = "What is it called when you make light change direction by passing it through a lens?";
			Questions[3] = "What percentage of the total number of known animals are invertebrates?";
			Questions[4] = "Bright�s Disease affects what part of the body?";
			Questions[5] = "What is the unit of measure used to quantify radioactive element activity?";
			Questions[6] = "What is the bone diseases that literally translates to �porous bones�?";
			Questions[7] = "Lagomorph, refers to what type of animal?";
			Questions[8] = "Most of a penny is made from what type of metal?";
			Questions[9] = "Which common flower can help to clean up radioactive waste?";
			Questions[10] = "Roughly how long does it take for light from the Sun to reach us here on Earth?";
			Questions[11] = "Roughly how much does a cumulus cloud weigh?";
			Questions[12] = "What is the study of Insects known as?";
			Questions[13] = "What is the fastest land snake in the world?";
			Questions[14] = "How many taste buds does a tongue have?";
			Questions[15] = "What blood type do you need to be a universal donor?";
			Questions[16] = "Which prefix is often used with scientific terms to indicate that something is the same, equal or constant ?";
			Questions[17] = "What is the branch of medical science which is concerned with the study ofdisease as it affects a community of people is called?";
			Questions[18] = "What famous scientist and inventor participated in the invention of the Aqua - Lung ?";
			Questions[19] = "Cystitis is the infection of which of the following?";

			/*Init Science Answers*/
			Answers[0][0] = "12%";
			Answers[0][1] = "5%";
			Answers[0][2] = "17%";
			Answers[0][3] = "20%";

			Answers[1][0] = "Temporal lobe";
			Answers[1][1] = "Frontal lobe";
			Answers[1][2] = "Parietal lobe";
			Answers[1][3] = "Occipital lobe";

			Answers[2][0] = "Refraction";
			Answers[2][1] = "Defraction";
			Answers[2][2] = "Reflection";
			Answers[2][3] = "Deflection";

			Answers[3][0] = "95%";
			Answers[3][1] = "99%";
			Answers[3][2] = "90%";
			Answers[3][3] = "92%";

			Answers[4][0] = "Kidneys";
			Answers[4][1] = "Brain";
			Answers[4][2] = "Eyes";
			Answers[4][3] = "Nervous Sytsem";

			Answers[5][0] = "Becqueral";
			Answers[5][1] = "Radians";
			Answers[5][2] = "Decquerals";
			Answers[5][3] = "Radicals";

			Answers[6][0] = "Osteoporosis";
			Answers[6][1] = "Porosis";
			Answers[6][2] = "Bioporis";
			Answers[6][3] = "Donporosis";

			Answers[7][0] = "Rabbits";
			Answers[7][1] = "Sheep";
			Answers[7][2] = "Lice";
			Answers[7][3] = "Hedgehogs";

			Answers[8][0] = "Zinc";
			Answers[8][1] = "Copper";
			Answers[8][2] = "Tin";
			Answers[8][3] = "Iron";

			Answers[9][0] = "Sunflower";
			Answers[9][1] = "Rose";
			Answers[9][2] = "Cauliflower";
			Answers[9][3] = "Tulip";

			Answers[10][0] = "8 minutes";
			Answers[10][1] = "2 seconds";
			Answers[10][2] = "7 minutes";
			Answers[10][3] = "1 minute";

			Answers[11][0] = "80 elephants";
			Answers[11][1] = "As much as a feather";
			Answers[11][2] = "Nothing";
			Answers[11][3] = "As much as 20 average adult humans";

			Answers[12][0] = "Entomology";
			Answers[12][1] = "Oncology";
			Answers[12][2] = "Petrology";
			Answers[12][3] = "Theology";

			Answers[13][0] = "Black Mamba";
			Answers[13][1] = "King Cobra";
			Answers[13][2] = "Anaconda";
			Answers[13][3] = "Viper";

			Answers[14][0] = "9,000";
			Answers[14][1] = "900";
			Answers[14][2] = "900,000";
			Answers[14][3] = "90,000";

			Answers[15][0] = "O-";
			Answers[15][1] = "AB+";
			Answers[15][2] = "AB-";
			Answers[15][3] = "O+";

			Answers[16][0] = "iso";
			Answers[16][1] = "ultra";
			Answers[16][2] = "meta";
			Answers[16][3] = "quasi";

			Answers[17][0] = "Epidemiology";
			Answers[17][1] = "Oncology";
			Answers[17][2] = "Paleontogy";
			Answers[17][3] = "Pathology";

			Answers[18][0] = "Jacques Cousteau";
			Answers[18][1] = "Leonardo da Vinci";
			Answers[18][2] = "Thomas Edison";
			Answers[18][3] = "Isaac Newton";

			Answers[19][0] = "Urinary Bladder";
			Answers[19][1] = "Liver";
			Answers[19][2] = "Pancreas";
			Answers[19][3] = "Lung";

			/*Init Science Correct Answers*/
			CorrectAnswer[0] = Answers[0][0];
			CorrectAnswer[1] = Answers[1][0];
			CorrectAnswer[2] = Answers[2][0];
			CorrectAnswer[3] = Answers[3][0];
			CorrectAnswer[4] = Answers[4][0];
			CorrectAnswer[5] = Answers[5][0];
			CorrectAnswer[6] = Answers[6][0];
			CorrectAnswer[7] = Answers[7][0];
			CorrectAnswer[8] = Answers[8][0];
			CorrectAnswer[9] = Answers[9][0];
			CorrectAnswer[10] = Answers[10][0];
			CorrectAnswer[11] = Answers[11][0];
			CorrectAnswer[12] = Answers[12][0];
			CorrectAnswer[13] = Answers[13][0];
			CorrectAnswer[14] = Answers[14][0];
			CorrectAnswer[15] = Answers[15][0];
			CorrectAnswer[16] = Answers[16][0];
			CorrectAnswer[17] = Answers[17][0];
			CorrectAnswer[18] = Answers[18][0];
			CorrectAnswer[19] = Answers[19][0];
		}
	}

	if (subject == Subjects[1])
	{
		if (set == 0)
		{
			/*Init Sports Questions*/
			Questions[0] = "How many soccer players should each team have on the field at the start of each match?";
			Questions[1] = "When Michael Jordan played for the Chicago Bulls, how many NBA Championships did he win?";
			Questions[2] = "Which Williams sister has won more Grand Slam titles?";
			Questions[3] = "What country won the very first FIFA World Cup in 1930?";
			Questions[4] = "In what year was the first-ever Wimbledon Championship held?";
			Questions[5] = "Which F1 racer holds the record for the most Grand Prix wins?";
			Questions[6] = "Which Jamaican sprinter won gold medals in the 100-metre and 200-metre races in three straight Olympic Games?";
			Questions[7] = "Which boxer was known as �The Greatest� and �The People�s Champion�?";
			Questions[8] = "What sport was Jesse Owens involved in?";
			Questions[9] = "What team gets the advantage of the first bat in baseball?";
			Questions[10] = "How many total minutes of action are there in an average baseball game?";
			Questions[11] = "What African country was the first ever to qualify for a World Cup?";
			Questions[12] = "In professional basketball, how high is the basketball hoop from the ground?";
			Questions[13] = "How many players are on a baseball team?";
			Questions[14] = "The Olympics are held every how many years?";
			Questions[15] = "During the first-ever modern Olympics, what were the first placers awarded with?";
			Questions[16] = "In American Football, a touchdown is worth how many points?";
			Questions[17] = "A sporting event is held every year on Memorial Day. What is it?";
			Questions[18] = "What is Canada�s national sport?";
			Questions[19] = "What year was the first ever Champions League Final held?";

			/*Init Sports Correct Answers*/
			Answers[0][0] = "11";
			Answers[0][1] = "12";
			Answers[0][2] = "9";
			Answers[0][3] = "7";

			Answers[1][0] = "6";
			Answers[1][1] = "7";
			Answers[1][2] = "8";
			Answers[1][3] = "10";

			Answers[2][0] = "Serena";
			Answers[2][1] = "Venus";
			Answers[2][2] = "Isha";
			Answers[2][3] = "Keisha";

			Answers[3][0] = "Uruguay";
			Answers[3][1] = "Argentina";
			Answers[3][2] = "France";
			Answers[3][3] = "Brazil";

			Answers[4][0] = "1877";
			Answers[4][1] = "1870";
			Answers[4][2] = "1876";
			Answers[4][3] = "1889";

			Answers[5][0] = "Lewis Hamilton";
			Answers[5][1] = "Michael Schumacher";
			Answers[5][2] = "Sebastian Vettel";
			Answers[5][3] = "Ayrton Senna";

			Answers[6][0] = "Usain Bolt";
			Answers[6][1] = "Yohan Blake";
			Answers[6][2] = "Asafa Powell";
			Answers[6][3] = "Warren Wer";

			Answers[7][0] = "Muhammad Ali";
			Answers[7][1] = "Tyson Fury";
			Answers[7][2] = "Anthony Joshua";
			Answers[7][3] = "Mike Tyson";

			Answers[8][0] = "Track and Field";
			Answers[8][1] = "Basketball";
			Answers[8][2] = "Tennis";
			Answers[8][3] = "Cycling";

			Answers[9][0] = "The visiting team";
			Answers[9][1] = "The home team";
			Answers[9][2] = "Team who flips heads";
			Answers[9][3] = "Team who flips tails";

			Answers[10][0] = "18";
			Answers[10][1] = "40";
			Answers[10][2] = "32";
			Answers[10][3] = "22";

			Answers[11][0] = "Egypt";
			Answers[11][1] = "Cameroon";
			Answers[11][2] = "Nigeria";
			Answers[11][3] = "South Africa";

			Answers[12][0] = "10ft";
			Answers[12][1] = "11ft";
			Answers[12][2] = "12ft";
			Answers[12][3] = "15ft";

			Answers[13][0] = "9";
			Answers[13][1] = "10";
			Answers[13][2] = "8";
			Answers[13][3] = "11";

			Answers[14][0] = "4";
			Answers[14][1] = "5";
			Answers[14][2] = "6";
			Answers[14][3] = "2";

			Answers[15][0] = "Sliver";
			Answers[15][1] = "Gold";
			Answers[15][2] = "Iron";
			Answers[15][3] = "Platinum";

			Answers[16][0] = "6";
			Answers[16][1] = "8";
			Answers[16][2] = "7";
			Answers[16][3] = "10";

			Answers[17][0] = "Indianapolis 500";
			Answers[17][1] = "Independence 500";
			Answers[17][2] = "Twenty Four Hour Race";
			Answers[17][3] = "Fifty Mile Run";

			Answers[18][0] = "Lacrosse";
			Answers[18][1] = "Skiing";
			Answers[18][2] = "Swimming";
			Answers[18][3] = "Ice Skating";

			Answers[19][0] = "1956";
			Answers[19][1] = "1976";
			Answers[19][2] = "1993";
			Answers[19][3] = "1966";

			/*Init Sports Correct Answers*/
			CorrectAnswer[0] = Answers[0][0];
			CorrectAnswer[1] = Answers[1][0];
			CorrectAnswer[2] = Answers[2][0];
			CorrectAnswer[3] = Answers[3][0];
			CorrectAnswer[4] = Answers[4][0];
			CorrectAnswer[5] = Answers[5][0];
			CorrectAnswer[6] = Answers[6][0];
			CorrectAnswer[7] = Answers[7][0];
			CorrectAnswer[8] = Answers[8][0];
			CorrectAnswer[9] = Answers[9][0];
			CorrectAnswer[10] = Answers[10][0];
			CorrectAnswer[11] = Answers[11][0];
			CorrectAnswer[12] = Answers[12][0];
			CorrectAnswer[13] = Answers[13][0];
			CorrectAnswer[14] = Answers[14][0];
			CorrectAnswer[15] = Answers[15][0];
			CorrectAnswer[16] = Answers[16][0];
			CorrectAnswer[17] = Answers[17][0];
			CorrectAnswer[18] = Answers[18][0];
			CorrectAnswer[19] = Answers[19][0];
		}

		if (set == 1)
		{
			/*Init Sports Questions*/
			Questions[0] = "Which hockey team did Wayne Gretzky play for in the 80's?";
			Questions[1] = "What�s the diameter of a basketball hoop in inches?";
			Questions[2] = "What do you call it when a player makes three back to back strikes in bowling?";
			Questions[3] = "What is the only country to have played in every single soccer World Cup?";
			Questions[4] = "This basketball move was banned from the years 1967 to 1976. What was it?";
			Questions[5] = "What sport were women allowed to play in the Olympics for the first time in 1912?";
			Questions[6] = "Who was the first-ever athlete to fail a drug test for the Olympics?";
			Questions[7] = "In the game of snooker, what is the highest possible break that a player can get?";
			Questions[8] = "What is a Yankee in the sport of horse racing?";
			Questions[9] = "What country was the first ever World Athletics Championship held?";
			Questions[10] = "In 1971, this boxer gave Muhammad Ali his first ever loss in professional boxing. What was his name?";
			Questions[11] = "How many F1 winning racers has Germany produced?";
			Questions[12] = "This country is known for producing the most Formula One World Championship winning racers. Which country is it?";
			Questions[13] = "How many championships did the Chicago Bulls win with Michael Jordan on their team?";
			Questions[14] = "What is the only team in the NFL to neither host nor play in the Super Bowl?";
			Questions[15] = "How many grand slams has Andy Murray won?";
			Questions[16] = "What event did Jessica Ennis-Hill win gold at the London 2012 Olympics?";
			Questions[17] = "Where is the World Darts Championship famously held every year?";
			Questions[18] = "Who is the Premier League�s all-time top scorer?";
			Questions[19] = "Which England footballer was famously never given a yellow card?";

			/*Init Sports Correct Answers*/
			Answers[0][0] = "Edmonton Oilers";
			Answers[0][1] = "Toronto Maple Leafs";
			Answers[0][2] = "Lake Placid";
			Answers[0][3] = "Pittsburgh Penguins";

			Answers[1][0] = "18";
			Answers[1][1] = "15";
			Answers[1][2] = "17";
			Answers[1][3] = "16";

			Answers[2][0] = "Turkey";
			Answers[2][1] = "Chicken";
			Answers[2][2] = "Bunny";
			Answers[2][3] = "Bull";

			Answers[3][0] = "Egypt";
			Answers[3][1] = "England";
			Answers[3][2] = "Cameroon";
			Answers[3][3] = "Brazil";

			Answers[4][0] = "Slam Dunk";
			Answers[4][1] = "Crossover";
			Answers[4][2] = "Between the Legs";
			Answers[4][3] = "Lay Up";

			Answers[5][0] = "Tennis";
			Answers[5][1] = "Hockey";
			Answers[5][2] = "Spirit ";
			Answers[5][3] = "Long Jump";

			Answers[6][0] = "Hans-Gunnar Liljenwall";
			Answers[6][1] = "Ndungwa Munguti";
			Answers[6][2] = "Ed Sheeran";
			Answers[6][3] = "Usain Bolt";

			Answers[7][0] = "155";
			Answers[7][1] = "200";
			Answers[7][2] = "250";
			Answers[7][3] = "185";

			Answers[8][0] = "11 individual bets";
			Answers[8][1] = "15 individual bets";
			Answers[8][2] = "12 individual bets";
			Answers[8][3] = "13 individual bets";

			Answers[9][0] = "Finland";
			Answers[9][1] = "Hungry";
			Answers[9][2] = "Greece";
			Answers[9][3] = "Italy";

			Answers[10][0] = "Joe Frazier";
			Answers[10][1] = "Tyson Fury";
			Answers[10][2] = "Lennox Lewis";
			Answers[10][3] = "Joe Louis";

			Answers[11][0] = "3";
			Answers[11][1] = "5";
			Answers[11][2] = "7";
			Answers[11][3] = "1";

			Answers[12][0] = "United Kingdom";
			Answers[12][1] = "Germany";
			Answers[12][2] = "France";
			Answers[12][3] = "Brazil";

			Answers[13][0] = "6";
			Answers[13][1] = "10";
			Answers[13][2] = "17";
			Answers[13][3] = "13";

			Answers[14][0] = "Cleveland Browns";
			Answers[14][1] = "Chicago Bears";
			Answers[14][2] = "Buffalo Bills";
			Answers[14][3] = "Miami Dolphins";

			Answers[15][0] = "3";
			Answers[15][1] = "6";
			Answers[15][2] = "7";
			Answers[15][3] = "2";

			Answers[16][0] = "Heptathlon";
			Answers[16][1] = "Track and Field";
			Answers[16][2] = "Tennis";
			Answers[16][3] = "100m Sprint";

			Answers[17][0] = "Alexandra Palace";
			Answers[17][1] = "Circus Tavern";
			Answers[17][2] = "Purfleet";
			Answers[17][3] = "Marshall Arena";

			Answers[18][0] = "Alan Shearer";
			Answers[18][1] = "Sergio Ag�ero";
			Answers[18][2] = "Andrew Cole";
			Answers[18][3] = "Wayne Rooney";

			Answers[19][0] = "Gary Lineker";
			Answers[19][1] = "Paul Scholes";
			Answers[19][2] = "Lee Bowyer";
			Answers[19][3] = "David Beckham";

			/*Init Sports Correct Answers*/
			CorrectAnswer[0] = Answers[0][0];
			CorrectAnswer[1] = Answers[1][0];
			CorrectAnswer[2] = Answers[2][0];
			CorrectAnswer[3] = Answers[3][0];
			CorrectAnswer[4] = Answers[4][0];
			CorrectAnswer[5] = Answers[5][0];
			CorrectAnswer[6] = Answers[6][0];
			CorrectAnswer[7] = Answers[7][0];
			CorrectAnswer[8] = Answers[8][0];
			CorrectAnswer[9] = Answers[9][0];
			CorrectAnswer[10] = Answers[10][0];
			CorrectAnswer[11] = Answers[11][0];
			CorrectAnswer[12] = Answers[12][0];
			CorrectAnswer[13] = Answers[13][0];
			CorrectAnswer[14] = Answers[14][0];
			CorrectAnswer[15] = Answers[15][0];
			CorrectAnswer[16] = Answers[16][0];
			CorrectAnswer[17] = Answers[17][0];
			CorrectAnswer[18] = Answers[18][0];
			CorrectAnswer[19] = Answers[19][0];
		}

		if (set == 2)
		{
			/*Init Sports Questions*/
			Questions[0] = "Who has won the most Sports Personality of the Year Awards?";
			Questions[1] = "Who is the longest serving head coach of the Scotland rugby team?";
			Questions[2] = "The Grand National, held at Aintree, is raced over what distance?";
			Questions[3] = "Who was the first Brit to win the Tour de France?";
			Questions[4] = "Who has won the least championships of the Rugby League in England?";
			Questions[5] = "Who is the top scorer for the England Women�s national football team?";
			Questions[6] = "Where did Sir Alex Ferguson start out his managerial career at?";
			Questions[7] = "In what year were the Commonwealth Games first held?";
			Questions[8] = "Which British golfer has won the most majors in their career ? ";
			Questions[9] = "What was the final score when England beat Australia in the rugby world cup final in 2003?";
			Questions[10] = "How many world titles has Phil �the Power� Talyor won in darts?";
			Questions[11] = "Who scored both goals in the final as Manchester United lifted Alex Ferguson�s first European trophy in 1991?";
			Questions[12] = "Which is the oldest golf club in England?";
			Questions[13] = "Who was the youngest English player to score a test century?";
			Questions[14] = "In which country would you find Eden Gardens cricket stadium?";
			Questions[15] = "How many runs are scored in a maiden over?";
			Questions[16] = "Which boxing trainer, who died in 2012, trained a record forty-one world champion fighters?";
			Questions[17] = "Which jockey earned his first Derby win aged thirty-six in 2007?";
			Questions[18] = "What was the first Horse racing course equipped with a photo-finish camera?";
			Questions[19] = "Who was the first woman to ride a winner over fences in Britain?";

			/*Init Sports Correct Answers*/
			Answers[0][0] = "Andy Murray";
			Answers[0][1] = "Lewis Hamilton";
			Answers[0][2] = "David Beckham";
			Answers[0][3] = "Ben Stokes";

			Answers[1][0] = "Gregor Townsend";
			Answers[1][1] = "Clermont Auvergne";
			Answers[1][2] = "Vern Cotter";
			Answers[1][3] = "Ross William Ford";

			Answers[2][0] = "4 miles";
			Answers[2][1] = "6 miles";
			Answers[2][2] = "8 miles";
			Answers[2][3] = "3 miles";

			Answers[3][0] = "Brian Robinson";
			Answers[3][1] = "Maurice Garin";
			Answers[3][2] = "Cadel Evans";
			Answers[3][3] = "Bradley Wiggins";

			Answers[4][0] = "Leigh Centurions";
			Answers[4][1] = "St Helens";
			Answers[4][2] = "Wigan";
			Answers[4][3] = "Brandford";

			Answers[5][0] = "Kelly Smith";
			Answers[5][1] = "Steph Houghton";
			Answers[5][2] = "Fara Williams";
			Answers[5][3] = "Tory Hopkin";

			Answers[6][0] = "East Stirlingshire";
			Answers[6][1] = "Aberdeen";
			Answers[6][2] = "Manchester United";
			Answers[6][3] = "Chesterfield United";

			Answers[7][0] = "1930";
			Answers[7][1] = "1932";
			Answers[7][2] = "1933";
			Answers[7][3] = "1928";

			Answers[8][0] = "Tom Watson";
			Answers[8][1] = "Jack Nicklaus";
			Answers[8][2] = "Nick Faldo";
			Answers[8][3] = "Rory Mcllroy";

			Answers[9][0] = "20-17";
			Answers[9][1] = "21-18";
			Answers[9][2] = "20-19";
			Answers[9][3] = "22-21";

			Answers[10][0] = "16";
			Answers[10][1] = "17";
			Answers[10][2] = "21";
			Answers[10][3] = "18";

			Answers[11][0] = "Mark Hughes";
			Answers[11][1] = "Mark Bright";
			Answers[11][2] = "Brain Kidd";
			Answers[11][3] = "Ryan Giggs";

			Answers[12][0] = "Two strokes";
			Answers[12][1] = "Blackheath Golf Club";
			Answers[12][2] = "The Old Links at Musselburgh";
			Answers[12][3] = "Walton Heath Golf Club";

			Answers[13][0] = "Denis Compton";
			Answers[13][1] = "Colin Cowdrey";
			Answers[13][2] = "Mushtaq Mohammad";
			Answers[13][3] = "Ben Stokes";

			Answers[14][0] = "India";
			Answers[14][1] = "England";
			Answers[14][2] = "Jamaica";
			Answers[14][3] = "Australia";

			Answers[15][0] = "0";
			Answers[15][1] = "10";
			Answers[15][2] = "30";
			Answers[15][3] = "100";

			Answers[16][0] = "Manny Steward";
			Answers[16][1] = "Naazim Richardson";
			Answers[16][2] = "Emanuel Steward";
			Answers[16][3] = "Leon Spinks";

			Answers[17][0] = "Frankie Dettori";
			Answers[17][1] = "Piggott";
			Answers[17][2] = "John R. Velazquez";
			Answers[17][3] = "Russell Blaze";

			Answers[18][0] = "Epsom";
			Answers[18][1] = "Del Mar Turf Club";
			Answers[18][2] = "Long Island";
			Answers[18][3] = "Santa Anita Racetrack";

			Answers[19][0] = "Jane Thorne";
			Answers[19][1] = "Katie Walsh";
			Answers[19][2] = "Milly Right";
			Answers[19][3] = "Katie Talbot";

			/*Init Sports Correct Answers*/
			CorrectAnswer[0] = Answers[0][0];
			CorrectAnswer[1] = Answers[1][0];
			CorrectAnswer[2] = Answers[2][0];
			CorrectAnswer[3] = Answers[3][0];
			CorrectAnswer[4] = Answers[4][0];
			CorrectAnswer[5] = Answers[5][0];
			CorrectAnswer[6] = Answers[6][0];
			CorrectAnswer[7] = Answers[7][0];
			CorrectAnswer[8] = Answers[8][0];
			CorrectAnswer[9] = Answers[9][0];
			CorrectAnswer[10] = Answers[10][0];
			CorrectAnswer[11] = Answers[11][0];
			CorrectAnswer[12] = Answers[12][0];
			CorrectAnswer[13] = Answers[13][0];
			CorrectAnswer[14] = Answers[14][0];
			CorrectAnswer[15] = Answers[15][0];
			CorrectAnswer[16] = Answers[16][0];
			CorrectAnswer[17] = Answers[17][0];
			CorrectAnswer[18] = Answers[18][0];
			CorrectAnswer[19] = Answers[19][0];
		}
	}

	if (subject == Subjects[2])
	{
		if (set == 0)
		{
			/*Init History Questions*/
			Questions[0] = "What was the first capital of ancient Egypt?";
			Questions[1] = "What fortification was built by the Romans around 122 A.D. across the width of Great Britain to separate Romans from the barbarians to the north?";
			Questions[2] = "How many of the Seven Wonders of the Ancient World still exist?";
			Questions[3] = "Who did Rome fight in the Punic Wars?";
			Questions[4] = "Which temple stands on the Acropolis in Athens?";
			Questions[5] = "The dynasty which ruled China for nearly 400 years?";
			Questions[6] = "What was the first city to reach a population of one million?";
			Questions[7] = "How long did the Hundred Years' War last?";
			Questions[8] = "Who was the first U.S. President to be impeached?";
			Questions[9] = "In what city did American colonists famously dump an entire shipment of tea into the harbour?";
			Questions[10] = "In which ocean was the Battle of Midway fought?";
			Questions[11] = "Who did Spartacus fight against in the Third Servile War?";
			Questions[12] = "Which Soviet satellite was the first to be launched into space in 1957?";
			Questions[13] = "How many British colonies formed the United States of America?";
			Questions[14] = "Who was the first Catholic pope?";
			Questions[15] = "What was the name of the military nobility of medieval and early modern Japan?";
			Questions[16] = "What year did the Great Depression begin?";
			Questions[17] = "The Nazi invasion of what country triggered World War II in Europe?";
			Questions[18] = "The Treaty of Versailles followed which war?";
			Questions[19] = "What military commander was undefeated?";

			/*Init Ancient History Correct Answers*/
			Answers[0][0] = "Memphis";
			Answers[0][1] = "Thebes";
			Answers[0][2] = "Alexandria";
			Answers[0][3] = "Baltimore";

			Answers[1][0] = "Hadrian�s Wall";
			Answers[1][1] = "Caesar's Wall";
			Answers[1][2] = "Cleon's Wall";
			Answers[1][3] = "Hannibal�s Wall";

			Answers[2][0] = "1";
			Answers[2][1] = "0";
			Answers[2][2] = "3";
			Answers[2][3] = "6";

			Answers[3][0] = "Carthage";
			Answers[3][1] = "Macedon";
			Answers[3][2] = "Briton";
			Answers[3][3] = "Persia";

			Answers[4][0] = "Parthenon";
			Answers[4][1] = "Pantheon";
			Answers[4][2] = "Colosseum";
			Answers[4][3] = "Temple to Hades";

			Answers[5][0] = "Han";
			Answers[5][1] = "Ming";
			Answers[5][2] = "Yin";
			Answers[5][3] = "Shu";

			Answers[6][0] = "Rome";
			Answers[6][1] = "Beijing";
			Answers[6][2] = "Carthago";
			Answers[6][3] = "Athens";

			Answers[7][0] = "116";
			Answers[7][1] = "100";
			Answers[7][2] = "99";
			Answers[7][3] = "112";

			Answers[8][0] = "Richard Nixon";
			Answers[8][1] = "Herbert Hoover";
			Answers[8][2] = "Bill Clinton";
			Answers[8][3] = "Donald Trump";

			Answers[9][0] = "Boston";
			Answers[9][1] = "New York";
			Answers[9][2] = "Baltimore";
			Answers[9][3] = "Charleston";

			Answers[10][0] = "Pacific";
			Answers[10][1] = "Southern";
			Answers[10][2] = "Atlantic";
			Answers[10][3] = "Indian";

			Answers[11][0] = "Romans";
			Answers[11][1] = "Persians";
			Answers[11][2] = "Carthaginians";
			Answers[11][3] = "Phoenicians";

			Answers[12][0] = "Sputnik";
			Answers[12][1] = "Mir";
			Answers[12][2] = "Vostok";
			Answers[12][3] = "Venera";

			Answers[13][0] = "13";
			Answers[13][1] = "12";
			Answers[13][2] = "24";
			Answers[13][3] = "16";

			Answers[14][0] = "St. Peter";
			Answers[14][1] = "St. John";
			Answers[14][2] = "St. Clement I";
			Answers[14][3] = "St. Linus";

			Answers[15][0] = "Samurai";
			Answers[15][1] = "Ninja";
			Answers[15][2] = "Nazakami";
			Answers[15][3] = "Kazoku";

			Answers[16][0] = "1929";
			Answers[16][1] = "1928";
			Answers[16][2] = "1931";
			Answers[16][3] = "1939";

			Answers[17][0] = "Poland";
			Answers[17][1] = "France";
			Answers[17][2] = "Austria";
			Answers[17][3] = "Denmark";

			Answers[18][0] = "World War 1";
			Answers[18][1] = "World War 2";
			Answers[18][2] = "French � Algerian War";
			Answers[18][3] = "Napoleonic Wars";

			Answers[19][0] = "Alexander the Great";
			Answers[19][1] = "Julius Caesar";
			Answers[19][2] = "William the Conqueror";
			Answers[19][3] = "Genghis Khan";

			/*Init Ancient History Correct Answers*/
			CorrectAnswer[0] = Answers[0][0];
			CorrectAnswer[1] = Answers[1][0];
			CorrectAnswer[2] = Answers[2][0];
			CorrectAnswer[3] = Answers[3][0];
			CorrectAnswer[4] = Answers[4][0];
			CorrectAnswer[5] = Answers[5][0];
			CorrectAnswer[6] = Answers[6][0];
			CorrectAnswer[7] = Answers[7][0];
			CorrectAnswer[8] = Answers[8][0];
			CorrectAnswer[9] = Answers[9][0];
			CorrectAnswer[10] = Answers[10][0];
			CorrectAnswer[11] = Answers[11][0];
			CorrectAnswer[12] = Answers[12][0];
			CorrectAnswer[13] = Answers[13][0];
			CorrectAnswer[14] = Answers[14][0];
			CorrectAnswer[15] = Answers[15][0];
			CorrectAnswer[16] = Answers[16][0];
			CorrectAnswer[17] = Answers[17][0];
			CorrectAnswer[18] = Answers[18][0];
			CorrectAnswer[19] = Answers[19][0];
		}

		if (set == 1)
		{
			/*Init Ancient History Questions*/
			Questions[0] = "Who was the first Prime Minister of the United Kingdom?";
			Questions[1] = "What war lasted approximately 38 minutes?";
			Questions[2] = "What year did Christopher Columbus set sail for the New World?";
			Questions[3] = "Who was emperor of Japan during World War II?";
			Questions[4] = "Cleopatra represented herself as the reincarnation of which Egyptian goddess?";
			Questions[5] = "Where was U.S. President John F. Kennedy assassinated?";
			Questions[6] = "What year did the California Gold Rush start?";
			Questions[7] = "Which war was ended by the 1763 Treaty of Paris?";
			Questions[8] = "What international body was established in 1945, immediately following World War II?";
			Questions[9] = "How many Punic Wars were there?";
			Questions[10] = "What empire began as an alliance of three city-states?";
			Questions[11] = "Who was Germany's first female Chancellor?";
			Questions[12] = "During the 'Summer of Love', where did hippies converge?";
			Questions[13] = "What was the first major war campaign fought entirely by air forces?";
			Questions[14] = "What was the name of Woodrow Wilson's plan for peace after World War I?";
			Questions[15] = "Differences between which two religions contributed to violent conflicts during the Indian struggle for independence in 1947?";
			Questions[16] = "Which of the following empires had no written language?";
			Questions[17] = "What tribe of Native Americans was the first to encounter the pilgrims?";
			Questions[18] = "How much of the world�s land mass did the British Empire cover?";
			Questions[19] = "Rome was founded which year?";

			/*Init Ancient History Correct Answers*/
			Answers[0][0] = "Robert Walpole";
			Answers[0][1] = "Henry Pelham";
			Answers[0][2] = "George Grenville";
			Answers[0][3] = "Winston Churchill";

			Answers[1][0] = "Anglo-Zanzibar War";
			Answers[1][1] = "War of 1812";
			Answers[1][2] = "Serbo-Bulgarian War";
			Answers[1][3] = "Falklands War";

			Answers[2][0] = "1492";
			Answers[2][1] = "1427";
			Answers[2][2] = "1467";
			Answers[2][3] = "1470";

			Answers[3][0] = "Hirohito";
			Answers[3][1] = "Yoshihito";
			Answers[3][2] = "Mutsuhito";
			Answers[3][3] = "Akihito";

			Answers[4][0] = "Isis";
			Answers[4][1] = "Nut";
			Answers[4][2] = "Bastet";
			Answers[4][3] = "Osiris";

			Answers[5][0] = "Dallas";
			Answers[5][1] = "Houston";
			Answers[5][2] = "Austin";
			Answers[5][3] = "San Antonio";

			Answers[6][0] = "1848";
			Answers[6][1] = "1813";
			Answers[6][2] = "1866";
			Answers[6][3] = "1890";

			Answers[7][0] = "The Seven Years War";
			Answers[7][1] = "The Hundred Years War";
			Answers[7][2] = "The Forty Years War";
			Answers[7][3] = "The Twelve Years War";

			Answers[8][0] = "United Nations";
			Answers[8][1] = "League of Nations";
			Answers[8][2] = "NATO";
			Answers[8][3] = "World Health Organisation";

			Answers[9][0] = "3";
			Answers[9][1] = "2";
			Answers[9][2] = "5";
			Answers[9][3] = "9";

			Answers[10][0] = "Aztec Empire";
			Answers[10][1] = "Roman Empire";
			Answers[10][2] = "Incan Empire";
			Answers[10][3] = "Mongol Empire";

			Answers[11][0] = "Angela Merkel";
			Answers[11][1] = "Janet Yellen";
			Answers[11][2] = "Maria Baum";
			Answers[11][3] = "Beatrix Von Storch";

			Answers[12][0] = "San Francisco";
			Answers[12][1] = "Los Angeles";
			Answers[12][2] = "Chicago";
			Answers[12][3] = "New York";

			Answers[13][0] = "The Battle of Britain";
			Answers[13][1] = "The Battle of Kursk";
			Answers[13][2] = "Operation Mole Cricket 19";
			Answers[13][3] = "Operation Linebacker";

			Answers[14][0] = "Fourteen Points";
			Answers[14][1] = "Sixteen Points";
			Answers[14][2] = "Twelve Points";
			Answers[14][3] = "Eighteen Points";

			Answers[15][0] = "Hinduism and Islam";
			Answers[15][1] = "Hinduism and Christianity";
			Answers[15][2] = "Islam and Christianity";
			Answers[15][3] = "Hinduism and Buddhism";

			Answers[16][0] = "Incan";
			Answers[16][1] = "Zulu";
			Answers[16][2] = "Aztec";
			Answers[16][3] = "Tang";

			Answers[17][0] = "Nauset";
			Answers[17][1] = "Mohican";
			Answers[17][2] = "Cherokee";
			Answers[17][3] = "Pequot";

			Answers[18][0] = "25%";
			Answers[18][1] = "20%";
			Answers[18][2] = "33%";
			Answers[18][3] = "10%";

			Answers[19][0] = "753 BC";
			Answers[19][1] = "987 BC";
			Answers[19][2] = "547 BC";
			Answers[19][3] = "639 BC";

			/*Init Ancient History Correct Answers*/
			CorrectAnswer[0] = Answers[0][0];
			CorrectAnswer[1] = Answers[1][0];
			CorrectAnswer[2] = Answers[2][0];
			CorrectAnswer[3] = Answers[3][0];
			CorrectAnswer[4] = Answers[4][0];
			CorrectAnswer[5] = Answers[5][0];
			CorrectAnswer[6] = Answers[6][0];
			CorrectAnswer[7] = Answers[7][0];
			CorrectAnswer[8] = Answers[8][0];
			CorrectAnswer[9] = Answers[9][0];
			CorrectAnswer[10] = Answers[10][0];
			CorrectAnswer[11] = Answers[11][0];
			CorrectAnswer[12] = Answers[12][0];
			CorrectAnswer[13] = Answers[13][0];
			CorrectAnswer[14] = Answers[14][0];
			CorrectAnswer[15] = Answers[15][0];
			CorrectAnswer[16] = Answers[16][0];
			CorrectAnswer[17] = Answers[17][0];
			CorrectAnswer[18] = Answers[18][0];
			CorrectAnswer[19] = Answers[19][0];
		}

		if (set == 2)
		{
			/*Init Ancient History Questions*/
			Questions[0] = "What captain claimed the first naval victory for the United States in the American Revolutionary War?";
			Questions[1] = "What Mesoamerican civilization was known for its hieroglyphic script?";
			Questions[2] = "Which of the following was a U.S. policy opposing European colonialism in the Americas?";
			Questions[3] = "What is the earliest surviving system of laws?";
			Questions[4] = "What was the last battle of the Napoleonic Wars?";
			Questions[5] = "How many of the 102 passengers aboard the Mayflower remained the winter after it set sail for the New World in 1620?";
			Questions[6] = "Who is generally considered the 'father of history'?";
			Questions[7] = "Who was the first European to discover New Zealand?";
			Questions[8] = "In what year did Constantinople fall to the Ottomans?";
			Questions[9] = "How many crusades into the �Holy Lands� where there between 1095 � 1291?";
			Questions[10] = "What did the Romans call Scotland?";
			Questions[11] = "Who is the Longest Reigning Monarch in History?";
			Questions[12] = "Which of these countries has never lost a war?";
			Questions[13] = "Who started the American Civil War?";
			Questions[14] = "Where did Rome lose three Legions, driving their Emperor to maddness with nightmares?";
			Questions[15] = "What was the name of the baker in whose bakery the Great Fire of London of 1666 apparently started?";
			Questions[16] = "The Spanish Civil War started in 1936 and ended in which year?";
			Questions[17] = "Who was the first to settle in what is now the United States?";
			Questions[18] = "What was the bloodiest Battle in History?";
			Questions[19] = "Which of the following wars had the highest casualty count?";

			/*Init Ancient History Correct Answers*/
			Answers[0][0] = "John Paul Jones";
			Answers[0][1] = "David Farragut";
			Answers[0][2] = "Oliver Hazard";
			Answers[0][3] = "Benedict Arnold";

			Answers[1][0] = "Maya";
			Answers[1][1] = "Olmec";
			Answers[1][2] = "Aztec";
			Answers[1][3] = "Inca";

			Answers[2][0] = "Monroe Doctrine";
			Answers[2][1] = "Truman Doctrine";
			Answers[2][2] = "Taft Doctrine";
			Answers[2][3] = "Jefferson Doctrine";

			Answers[3][0] = "The Code of Hammurabi";
			Answers[3][1] = "Rosetta Stone";
			Answers[3][2] = "Shabaka Stone";
			Answers[3][3] = "Hebrew Torah";

			Answers[4][0] = "Battle of Wavre";
			Answers[4][1] = "Battle of Waterloo";
			Answers[4][2] = "Battle of Trafalgar";
			Answers[4][3] = "Battle of the Nile";

			Answers[5][0] = "53";
			Answers[5][1] = "102";
			Answers[5][2] = "80";
			Answers[5][3] = "45";

			Answers[6][0] = "Herodotus";
			Answers[6][1] = "Xenophon";
			Answers[6][2] = "Thucydides";
			Answers[6][3] = "Aristotle";

			Answers[7][0] = "Abel Janszoon Tasman";
			Answers[7][1] = "James Cook";
			Answers[7][2] = "Marc-Joseph Marion";
			Answers[7][3] = "Marco Polo";

			Answers[8][0] = "1453";
			Answers[8][1] = "1543";
			Answers[8][2] = "1534";
			Answers[8][3] = "1435";

			Answers[9][0] = "9";
			Answers[9][1] = "13";
			Answers[9][2] = "5";
			Answers[9][3] = "7";

			Answers[10][0] = "Caledonia";
			Answers[10][1] = "Scots Land";
			Answers[10][2] = "Alba";
			Answers[10][3] = "Scotoma";

			Answers[11][0] = "Louis XIV of France";
			Answers[11][1] = "Elizabeth II of England";
			Answers[11][2] = "Johann II of Liechtenstein";
			Answers[11][3] = "Constantine VIII of the Byzantine Empire";

			Answers[12][0] = "Pakistan";
			Answers[12][1] = "USA";
			Answers[12][2] = "Belgium";
			Answers[12][3] = "Holland";

			Answers[13][0] = "Confederates";
			Answers[13][1] = "Unionists";
			Answers[13][2] = "Mutual Declaration";
			Answers[13][3] = "No one Knows for sure";

			Answers[14][0] = "Teutoburg Forest, Germania";
			Answers[14][1] = "Galloway Forest, Caledonia";
			Answers[14][2] = "Carrhae, Mesopotamia";
			Answers[14][3] = "Kielder Forest, Britannia";

			Answers[15][0] = "Thomas Farriner";
			Answers[15][1] = "Thomas Cook";
			Answers[15][2] = "Thomas Farday";
			Answers[15][3] = "Thomas Baker";

			Answers[16][0] = "1939";
			Answers[16][1] = "1945";
			Answers[16][2] = "1937";
			Answers[16][3] = "1942";

			Answers[17][0] = "Spain";
			Answers[17][1] = "Britain";
			Answers[17][2] = "Portugal";
			Answers[17][3] = "France";

			Answers[18][0] = "Sacking of Baghdad, 1258";
			Answers[18][1] = "Brusilov, 1916";
			Answers[18][2] = "The Somme, 1916";
			Answers[18][3] = "Gettysburg, 1863";

			Answers[19][0] = "Three Kingdoms War, China";
			Answers[19][1] = "Mongol Conquests, Eurasia";
			Answers[19][2] = "World War 1, Worldwide";
			Answers[19][3] = "Yellow Turban Rebellion, China";

			/*Init Ancient History Correct Answers*/
			CorrectAnswer[0] = Answers[0][0];
			CorrectAnswer[1] = Answers[1][0];
			CorrectAnswer[2] = Answers[2][0];
			CorrectAnswer[3] = Answers[3][0];
			CorrectAnswer[4] = Answers[4][0];
			CorrectAnswer[5] = Answers[5][0];
			CorrectAnswer[6] = Answers[6][0];
			CorrectAnswer[7] = Answers[7][0];
			CorrectAnswer[8] = Answers[8][0];
			CorrectAnswer[9] = Answers[9][0];
			CorrectAnswer[10] = Answers[10][0];
			CorrectAnswer[11] = Answers[11][0];
			CorrectAnswer[12] = Answers[12][0];
			CorrectAnswer[13] = Answers[13][0];
			CorrectAnswer[14] = Answers[14][0];
			CorrectAnswer[15] = Answers[15][0];
			CorrectAnswer[16] = Answers[16][0];
			CorrectAnswer[17] = Answers[17][0];
			CorrectAnswer[18] = Answers[18][0];
			CorrectAnswer[19] = Answers[19][0];
		}
	}

	if (subject == Subjects[3])
	{
		if (set == 0)
		{
			/*Init Movies & Television Questions*/
			Questions[0] = "Who plays the Prime Minister in Love Actually?";
			Questions[1] = "In the Avengers movies, what is the name of Thor's hammer?";
			Questions[2] = "What is the name of the second James Bond film?";
			Questions[3] = "What is the name of the spell used by Ron and Hermione in Harry Potter and the Philosopher�s Stone to make their feathers fly?";
			Questions[4] = "�Frankly my dear, I don�t give a damn� is an iconic line from which classic film?";
			Questions[5] = "What does Tom Hanks compare life to in Forrest Gump?";
			Questions[6] = "Who directed the movies: Titanic, Avatar and The Terminator?";
			Questions[7] = "Which Scottish singer regularly played herself in Absolutely Fabulous?";
			Questions[8] = "Who plays Jamie Fraser in Outlander?";
			Questions[9] = "Which movie was incorrectly announced as the winner of Best Picture at the 2017 Academy Awards?";
			Questions[10] = "In Beauty and the Beast, which wing of the castle is Belle forbidden from visiting?";
			Questions[11] = "who is Ewan McGregor's character in Trainspotting?";
			Questions[12] = "For what movie did Steven Spielberg win his first Oscar for Best Director?";
			Questions[13] = "Crime thriller Luther starring Idris Elba first aired in which year?";
			Questions[14] = "Which Life of Brian song did the Monty Python cast reach Number 3 in the UK charts in 1991?";
			Questions[15] = "Who played Mrs. Robinson in The Graduate?";
			Questions[16] = "Who directed Parasite � the first foreign language film to win the Academy Award for Best Picture?";
			Questions[17] = "Which film franchise holds the Guinness World Record for the most false feet used by a costume department?";
			Questions[18] = "Which Scottish actor played the 10th Doctor in Doctor Who?";
			Questions[19] = "�There's been a murder!� is a quote from which TV show?";

			/*Init Movies & Television Correct Answers*/
			Answers[0][0] = "Hugh Grant";
			Answers[0][1] = "Andrew Lincoln";
			Answers[0][2] = "Chiwetel Ejiofor";
			Answers[0][3] = "Tom Hiddleston";

			Answers[1][0] = "Mjolnir";
			Answers[1][1] = "Lighting Hammer";
			Answers[1][2] = "Stormbreaker";
			Answers[1][3] = "Stormbringer";

			Answers[2][0] = "From Russia with Love";
			Answers[2][1] = "Dr.No";
			Answers[2][2] = "Goldfinger";
			Answers[2][3] = "No Time to Die";

			Answers[3][0] = "Levitation Charm";
			Answers[3][1] = "Aberto";
			Answers[3][2] = "Expecto patronum";
			Answers[3][3] = "Accio";

			Answers[4][0] = "Gone With the Wind";
			Answers[4][1] = "The Wizard of Oz";
			Answers[4][2] = "Casablanca";
			Answers[4][3] = "Cleopatra";

			Answers[5][0] = "A Box of chocolates";
			Answers[5][1] = "Flowers";
			Answers[5][2] = "Pie";
			Answers[5][3] = "Pandora�s Box";

			Answers[6][0] = "James Cameron";
			Answers[6][1] = "Jamie Lee Curtis";
			Answers[6][2] = "Steven Spielberg";
			Answers[6][3] = "Michael Bay";

			Answers[7][0] = "Lulu Kennedy-Cairns";
			Answers[7][1] = "Jennifer Saunders";
			Answers[7][2] = "Joanna Lumley";
			Answers[7][3] = "Annie Lennox";

			Answers[8][0] = "Sam Heughan";
			Answers[8][1] = "Caitriona Balfe";
			Answers[8][2] = "Sophie Skelton";
			Answers[8][3] = "Tobias Menzies";

			Answers[9][0] = "Moonlight";
			Answers[9][1] = "LA LA LAND";
			Answers[9][2] = "Hidden Figures";
			Answers[9][3] = "Hacksaw Ridge";

			Answers[10][0] = "The West Wing";
			Answers[10][1] = "Ch�teau de Chambord";
			Answers[10][2] = "The East Wing";
			Answers[10][3] = "The Basement";

			Answers[11][0] = "Mark Renton";
			Answers[11][1] = "Sir Alec Guinness";
			Answers[11][2] = "Obi-Wan Kenobi";
			Answers[11][3] = "Fargo";

			Answers[12][0] = "Schindler�s List";
			Answers[12][1] = "Saving Private Ryan";
			Answers[12][2] = "Munich";
			Answers[12][3] = "Transformers";

			Answers[13][0] = "2010";
			Answers[13][1] = "2009";
			Answers[13][2] = "2012";
			Answers[13][3] = "2008";

			Answers[14][0] = "Always Look on the Bright Side of Life";
			Answers[14][1] = "Lumberjack Song";
			Answers[14][2] = "Accountancy Shanty";
			Answers[14][3] = "Every Sperm Is Sacred";

			Answers[15][0] = "Anne Bancroft";
			Answers[15][1] = "Alice Ghostley";
			Answers[15][2] = "Katharine Ross";
			Answers[15][3] = "Diana Ross";

			Answers[16][0] = "Bong Joon Ho ";
			Answers[16][1] = "Thomas Vinterberg";
			Answers[16][2] = "Chlo� Zhao";
			Answers[16][3] = "Timothy Zhan";

			Answers[17][0] = "The Lord of the Rings";
			Answers[17][1] = "Star Wars";
			Answers[17][2] = "Pirates of the Caribbean";
			Answers[17][3] = "Star Trek";

			Answers[18][0] = "David Tennant";
			Answers[18][1] = "Matt Smith";
			Answers[18][2] = "Peter Capaldi";
			Answers[18][3] = "Ewan McGregor";

			Answers[19][0] = "Taggart";
			Answers[19][1] = "Murder She Wrote";
			Answers[19][2] = "Harrow";
			Answers[19][3] = "How to get Away with Murder";

			/*Init Movies & Television Correct Answers*/
			CorrectAnswer[0] = Answers[0][0];
			CorrectAnswer[1] = Answers[1][0];
			CorrectAnswer[2] = Answers[2][0];
			CorrectAnswer[3] = Answers[3][0];
			CorrectAnswer[4] = Answers[4][0];
			CorrectAnswer[5] = Answers[5][0];
			CorrectAnswer[6] = Answers[6][0];
			CorrectAnswer[7] = Answers[7][0];
			CorrectAnswer[8] = Answers[8][0];
			CorrectAnswer[9] = Answers[9][0];
			CorrectAnswer[10] = Answers[10][0];
			CorrectAnswer[11] = Answers[11][0];
			CorrectAnswer[12] = Answers[12][0];
			CorrectAnswer[13] = Answers[13][0];
			CorrectAnswer[14] = Answers[14][0];
			CorrectAnswer[15] = Answers[15][0];
			CorrectAnswer[16] = Answers[16][0];
			CorrectAnswer[17] = Answers[17][0];
			CorrectAnswer[18] = Answers[18][0];
			CorrectAnswer[19] = Answers[19][0];
		}

		if (set == 1)
		{
			/*Init Movies & Television Questions*/
			Questions[0] = "What are the dying words of Charles Foster Kane in Citizen Kane?";
			Questions[1] = "How many Academy Awards has Leonardo DiCaprio won?";
			Questions[2] = "What�s the name of the skyscraper in Die Hard?";
			Questions[3] = "Which member of The Beatles narrated the first series of Thomas the Tank Engine on TV?";
			Questions[4] = "What flavour of Pop Tarts does Buddy the Elf use in his spaghetti in Elf?";
			Questions[5] = "Which Blackadder character always had a �cunning plan�?";
			Questions[6] = "What Wes Craven horror movie carried the tagline, �To avoid fainting, keep repeating, It�s only a movie'?";
			Questions[7] = "What was the name of the character in Harry Potter played by Shirley Henderson?";
			Questions[8] = "Tom Hanks won two consecutive Oscars in 1994 and 1995. What was the 1994 film?";
			Questions[9] = "What pop vocal group performs at the wedding in Bridesmaids?";
			Questions[10] = "What is the main pub called in Peaky Blinders where the Shelby�s meet?";
			Questions[11] = "Who played Queen Elizabeth II in the first two seasons of The Crown?";
			Questions[12] = "Which sauce is Rick famously obsessed with in Rick and Morty?";
			Questions[13] = "Which director, who died in 2009, was known for his 80s teen movies including The Breakfast Club and Pretty In Pink?";
			Questions[14] = "What American writer/director starred in several iconic European-produced �Spaghetti Westerns�?";
			Questions[15] = "who are the creators of Stranger Things?";
			Questions[16] = "What 1976 thriller does Robert De Niro famously say �You talkin� to me?�";
			Questions[17] = "Mary Poppins is the nanny to which family?";
			Questions[18] = "What is the name of Jack Sparrow's ship?";
			Questions[19] = "What�s the name of the anthemic dance near the beginning of The Rocky Horror Picture Show?";

			/*Init Movies & Television Correct Answers*/
			Answers[0][0] = "Rosebud";
			Answers[0][1] = "Save Me";
			Answers[0][2] = "No";
			Answers[0][3] = "Why";

			Answers[1][0] = "1";
			Answers[1][1] = "4";
			Answers[1][2] = "2";
			Answers[1][3] = "0";

			Answers[2][0] = "Nakatomi Plaza";
			Answers[2][1] = "The pearl";
			Answers[2][2] = "Keyscraper";
			Answers[2][3] = "Katomi Tower";

			Answers[3][0] = "Ringo Starr";
			Answers[3][1] = "John Lennon";
			Answers[3][2] = "Paul McCartney";
			Answers[3][3] = "George Harrison";

			Answers[4][0] = "Chocolate";
			Answers[4][1] = "Strawberry";
			Answers[4][2] = "Villain";
			Answers[4][3] = "Lemon";

			Answers[5][0] = "Baldrick";
			Answers[5][1] = "Melchett";
			Answers[5][2] = "Lord Percy Percy";
			Answers[5][3] = "Edmund blackadder";

			Answers[6][0] = "The Last House on the Left";
			Answers[6][1] = "The Haunted House Next Door";
			Answers[6][2] = "Castle Black";
			Answers[6][3] = "Sinister";

			Answers[7][0] = "Moaning Myrtle";
			Answers[7][1] = "Minerva McGonagall";
			Answers[7][2] = "Luna Lovegood";
			Answers[7][3] = "Ginny Weasley";

			Answers[8][0] = "Philadelphia";
			Answers[8][1] = "Taggart";
			Answers[8][2] = "Forrest Gump";
			Answers[8][3] = "Big";

			Answers[9][0] = "Wilson Phillips";
			Answers[9][1] = "ABBA";
			Answers[9][2] = "Queen";
			Answers[9][3] = "POP";

			Answers[10][0] = "The Garrison Tavern";
			Answers[10][1] = "The Victoria";
			Answers[10][2] = "The Rose Villa Tavern";
			Answers[10][3] = "The Rose Garrison";

			Answers[11][0] = "Claire Foy";
			Answers[11][1] = "Olivia Colman";
			Answers[11][2] = "Vanessa Kirby";
			Answers[11][3] = "Elizabeth Christy";

			Answers[12][0] = "Szechaun";
			Answers[12][1] = "Fire Sauce";
			Answers[12][2] = "Firekracker";
			Answers[12][3] = "Scizzler";

			Answers[13][0] = "John Hughes";
			Answers[13][1] = "Jane Richardson";
			Answers[13][2] = "Natasha Richardson";
			Answers[13][3] = "Casey Lee";

			Answers[14][0] = "Clint Eastwood";
			Answers[14][1] = "Steven Spielbery";
			Answers[14][2] = "Mzartain Scorese";
			Answers[14][3] = "Ronald Regan";

			Answers[15][0] = "The Duffer Brothers";
			Answers[15][1] = "The Richardson Brothers";
			Answers[15][2] = "The Davidson Brothers";
			Answers[15][3] = "The Dudley Brothers";

			Answers[16][0] = "Taxi Driver";
			Answers[16][1] = "Marathon Man";
			Answers[16][2] = "The Cassandra Crossing";
			Answers[16][3] = "The Godfather: Part 2";

			Answers[17][0] = "The Banks family";
			Answers[17][1] = "The Bright family";
			Answers[17][2] = "The Richardson family";
			Answers[17][3] = "The Dorsley family";

			Answers[18][0] = "Black Pearl";
			Answers[18][1] = "Queen Anne's Revenge";
			Answers[18][2] = "Flying Dutchman";
			Answers[18][3] = "The Pearl";

			Answers[19][0] = "The Time Wrap";
			Answers[19][1] = "Street Dance";
			Answers[19][2] = "Classic Stomp";
			Answers[19][3] = "Sinister Step";

			/*Init Movies & Television Correct Answers*/
			CorrectAnswer[0] = Answers[0][0];
			CorrectAnswer[1] = Answers[1][0];
			CorrectAnswer[2] = Answers[2][0];
			CorrectAnswer[3] = Answers[3][0];
			CorrectAnswer[4] = Answers[4][0];
			CorrectAnswer[5] = Answers[5][0];
			CorrectAnswer[6] = Answers[6][0];
			CorrectAnswer[7] = Answers[7][0];
			CorrectAnswer[8] = Answers[8][0];
			CorrectAnswer[9] = Answers[9][0];
			CorrectAnswer[10] = Answers[10][0];
			CorrectAnswer[11] = Answers[11][0];
			CorrectAnswer[12] = Answers[12][0];
			CorrectAnswer[13] = Answers[13][0];
			CorrectAnswer[14] = Answers[14][0];
			CorrectAnswer[15] = Answers[15][0];
			CorrectAnswer[16] = Answers[16][0];
			CorrectAnswer[17] = Answers[17][0];
			CorrectAnswer[18] = Answers[18][0];
			CorrectAnswer[19] = Answers[19][0];
		}

		if (set == 2)
		{
			/*Init Movies & Television Questions*/
			Questions[0] = "Who played the lead role in the 2001 film Lara Croft: Tomb Raider?";
			Questions[1] = "What is Zoolander's first name?";
			Questions[2] = "The first episode of Downton Abbey begins on the same day as which real historic event?";
			Questions[3] = "Sunset Boulevard was named after the road of the same name. In which city can this road be found?";
			Questions[4] = "How many James Bond films did Sean Connery play the 007 agent?";
			Questions[5] = "Which film which boasts the famous line: �You can�t handle the truth!�?";
			Questions[6] = "In Spiceworld: The Movie, the girls will star in Spice Force Five. Which Quentin Tarantino movie is this a reference to?";
			Questions[7] = "�Well, nobody�s perfect� is the final line from what classic 1959 comedy starring Marilyn Monroe?";
			Questions[8] = "Aaron Sorkin won an Oscar for writing the 2010 drama about the creation of Facebook?";
			Questions[9] = "What animated classic was the first film of the late-twentieth-century �Disney Renaissance�?";
			Questions[10] = "In The Young Ones, what is the name of the college the four guys attend?";
			Questions[11] = "What is the name of the opening number from the 2016 musical, La La Land?";
			Questions[12] = "what is the highest grossing film franchise of all time?";
			Questions[13] = "Who wrote the score for 1994 Disney film The Lion King?";
			Questions[14] = "When was the National Television Awards� Most Popular Entertainment/TV Presenter category won by someone other than Ant and Dec?";
			Questions[15] = "What is the name of the Christmas hit written by Will Brewis� father in the comedy About a Boy?";
			Questions[16] = "What Film Was So Scary Theatre Staff (Supposedly) Carried Smelling Salts For Guests?";
			Questions[17] = "How Much Power Does It Take To Fuel The Flux Capacitor In Back To The Future?";
			Questions[18] = "What Was The First Horror Movie To Win An Oscar?";
			Questions[19] = "What Is The Number On The Roof Of The Bus In Speed?";

			/*Init Movies & Television Correct Answers*/
			Answers[0][0] = "Angelina Jolie";
			Answers[0][1] = "Alicia Vikander";
			Answers[0][2] = "Kristin Scott Thomas";
			Answers[0][3] = "Charlotte Dawson";

			Answers[1][0] = "Derek";
			Answers[1][1] = "Dan";
			Answers[1][2] = "Peter";
			Answers[1][3] = "Zhan";

			Answers[2][0] = "The Sinking of the Titanic";
			Answers[2][1] = "Death of Elizabeth I";
			Answers[2][2] = "Birth of Elizabeth II";
			Answers[2][3] = "World War 1 Ends";

			Answers[3][0] = "Los Angeles ";
			Answers[3][1] = "New York";
			Answers[3][2] = "Chicago";
			Answers[3][3] = "San Francisco";

			Answers[4][0] = "6";
			Answers[4][1] = "7";
			Answers[4][2] = "8";
			Answers[4][3] = "10";

			Answers[5][0] = "A Few Good Men ";
			Answers[5][1] = "A Time to Kill";
			Answers[5][2] = "Top Gun";
			Answers[5][3] = "JAG";

			Answers[6][0] = "Pulp Fiction";
			Answers[6][1] = "Kill Bill";
			Answers[6][2] = "Inglorious Basterds";
			Answers[6][3] = "Once Upon A Time in Hollywood";

			Answers[7][0] = "Some Like it Hot";
			Answers[7][1] = "The Seven Year Itch";
			Answers[7][2] = "Gentlemen Prefer Blondes";
			Answers[7][3] = "The Misfits";

			Answers[8][0] = "The social Network";
			Answers[8][1] = "Unfriended";
			Answers[8][2] = "The Network";
			Answers[8][3] = "Facebook";

			Answers[9][0] = "The Little Mermaid";
			Answers[9][1] = "Snow White and the Seven Dwarfs";
			Answers[9][2] = "Pinocchio";
			Answers[9][3] = "Lion King";

			Answers[10][0] = "Scumbag College";
			Answers[10][1] = "Albemarle College";
			Answers[10][2] = "Coldharbour";
			Answers[10][3] = "MIT";

			Answers[11][0] = "Another Day of Sun";
			Answers[11][1] = "City Of Stars";
			Answers[11][2] = "Someone in the Crowd";
			Answers[11][3] = "Flashing Lights";

			Answers[12][0] = "Marvel Cinematic Universe";
			Answers[12][1] = "Star Wars";
			Answers[12][2] = "DC Universe";
			Answers[12][3] = "The Wizarding World ";

			Answers[13][0] = "Hans Zimmer";
			Answers[13][1] = "Elton John";
			Answers[13][2] = "John Williams";
			Answers[13][3] = "Danny Elfmann";

			Answers[14][0] = "2000";
			Answers[14][1] = "2004";
			Answers[14][2] = "2010";
			Answers[14][3] = "2017";

			Answers[15][0] = "Santa�s Super Sleigh";
			Answers[15][1] = "Santa Come";
			Answers[15][2] = "Last Night Christmas";
			Answers[15][3] = "Jingle Bell Rock";

			Answers[16][0] = "Phantom of the Opera";
			Answers[16][1] = "The Texas Chainsaw Massacre";
			Answers[16][2] = "The Exorcist";
			Answers[16][3] = "The Masked Crusader";

			Answers[17][0] = "1.21 Gigawatts";
			Answers[17][1] = "201.21 Gigawatts";
			Answers[17][2] = "11.21 Gigawatts";
			Answers[17][3] = "3.21 Gigawatts";

			Answers[18][0] = "Silence of the Lambs";
			Answers[18][1] = "Get Out";
			Answers[18][2] = "Black Swan";
			Answers[18][3] = "IT";

			Answers[19][0] = "2525";
			Answers[19][1] = "2345";
			Answers[19][2] = "1796";
			Answers[19][3] = "1352";

			/*Init Movies & Television Correct Answers*/
			CorrectAnswer[0] = Answers[0][0];
			CorrectAnswer[1] = Answers[1][0];
			CorrectAnswer[2] = Answers[2][0];
			CorrectAnswer[3] = Answers[3][0];
			CorrectAnswer[4] = Answers[4][0];
			CorrectAnswer[5] = Answers[5][0];
			CorrectAnswer[6] = Answers[6][0];
			CorrectAnswer[7] = Answers[7][0];
			CorrectAnswer[8] = Answers[8][0];
			CorrectAnswer[9] = Answers[9][0];
			CorrectAnswer[10] = Answers[10][0];
			CorrectAnswer[11] = Answers[11][0];
			CorrectAnswer[12] = Answers[12][0];
			CorrectAnswer[13] = Answers[13][0];
			CorrectAnswer[14] = Answers[14][0];
			CorrectAnswer[15] = Answers[15][0];
			CorrectAnswer[16] = Answers[16][0];
			CorrectAnswer[17] = Answers[17][0];
			CorrectAnswer[18] = Answers[18][0];
			CorrectAnswer[19] = Answers[19][0];
		}
	}

	if (subject == Subjects[4])
	{
		if (set == 0)
		{
			/*Init Music Questions*/
			Questions[0] = "What is the name of The Beatles� first album?";
			Questions[1] = "Which mathematical symbol was the title of Ed Sheeran�s first album in 2011?";
			Questions[2] = "What is the name of Miley Cyrus� father?";
			Questions[3] = "What was the name of Tom Jones� first ever single?";
			Questions[4] = "Which former One Direction member released 'Watermelon Sugar' and 'Adore You'?";
			Questions[5] = "Which Canadian artist released 'Man! I Feel Like A Woman!� in 1999?";
			Questions[6] = "Where did Stormzy work before becoming a famous musician?";
			Questions[7] = "In which year did Little Mix win The X Factor?";
			Questions[8] = "In which decade did Elvis have his first UK No. 1?";
			Questions[9] = "'Crazy in Love' was the first solo No. 1 for which singer?";
			Questions[10] = "Who was the first to leave Take That?";
			Questions[11] = "Which group wrote most of the songs for the movie Saturday Night Fever?";
			Questions[12] = "Who wrote the song 'Let's Get It On'?";
			Questions[13] = "According to the lyrics of Kelis' 'Milkshake', released in 2003, where does her milkshake bring all the boys?";
			Questions[14] = "What is David Bowie's real surname?";
			Questions[15] = "'You Spin Me Round (Like a Record)' is a song by which British band?";
			Questions[16] = "What instrument did Phil Collins play when he was a member of Genesis?";
			Questions[17] = "Who was the youngest Beatle?";
			Questions[18] = "How old was Mozart when he wrote his first piece?";
			Questions[19] = "Who is the only member of ZZ Top that doesn't have a beard?";

			/*Init Music Correct Answers*/
			Answers[0][0] = "Please Please Me";
			Answers[0][1] = "Abbey Road";
			Answers[0][2] = "Help!";
			Answers[0][3] = "Let It Be";

			Answers[1][0] = "+";
			Answers[1][1] = "-";
			Answers[1][2] = "%";
			Answers[1][3] = "=";

			Answers[2][0] = "Billy Ray";
			Answers[2][1] = "Ben";
			Answers[2][2] = "David";
			Answers[2][3] = "Dave";

			Answers[3][0] = "Chills and Fever";
			Answers[3][1] = "Deliah";
			Answers[3][2] = "Sexbomb";
			Answers[3][3] = "It's Not Unusual";

			Answers[4][0] = "Harry Styles";
			Answers[4][1] = "Louis Tomlinson";
			Answers[4][2] = "Laim Payne";
			Answers[4][3] = "Zayn Malik";

			Answers[5][0] = "Shania Twain";
			Answers[5][1] = "Avril Lavigne";
			Answers[5][2] = "Caline Dion";
			Answers[5][3] = "Carly Rae Jepsen";

			Answers[6][0] = "An Oil Refinery ";
			Answers[6][1] = "YouTube";
			Answers[6][2] = "A Cinema";
			Answers[6][3] = "A Toy Factory";

			Answers[7][0] = "2011";
			Answers[7][1] = "2012";
			Answers[7][2] = "2010";
			Answers[7][3] = "2009";

			Answers[8][0] = "1950�s";
			Answers[8][1] = "1980�s";
			Answers[8][2] = "1960�s";
			Answers[8][3] = "1970�s";

			Answers[9][0] = "Beyonce";
			Answers[9][1] = "Cheryl Cole";
			Answers[9][2] = "Lady Gaga";
			Answers[9][3] = "Rihanna";

			Answers[10][0] = "Robbie Williams";
			Answers[10][1] = "Gary Barlow";
			Answers[10][2] = "Mark Owen";
			Answers[10][3] = "Howard Donald";

			Answers[11][0] = "The Bee Gees";
			Answers[11][1] = "Tavares";
			Answers[11][2] = "David Shire";
			Answers[11][3] = "The Beatles";

			Answers[12][0] = "Marvin Gaye";
			Answers[12][1] = "Nona Gaye";
			Answers[12][2] = "Marvin Gayne";
			Answers[12][3] = "Michael Bubl�";

			Answers[13][0] = "To the Yard";
			Answers[13][1] = "To the Field";
			Answers[13][2] = "To the Bed";
			Answers[13][3] = "To the House";

			Answers[14][0] = "Jones";
			Answers[14][1] = "Davidson";
			Answers[14][2] = "Bowie";
			Answers[14][3] = "Dave";

			Answers[15][0] = "Dead or Alive";
			Answers[15][1] = "Culture Club";
			Answers[15][2] = "Bananarama";
			Answers[15][3] = "Spice Girls";

			Answers[16][0] = "Drum";
			Answers[16][1] = "Keyboard";
			Answers[16][2] = "Flute";
			Answers[16][3] = "Guitar";

			Answers[17][0] = "George Harrison";
			Answers[17][1] = "John Lennon";
			Answers[17][2] = "Ringo Starr";
			Answers[17][3] = "Paul McCartney";

			Answers[18][0] = "5";
			Answers[18][1] = "15";
			Answers[18][2] = "7";
			Answers[18][3] = "10";

			Answers[19][0] = "Frank Beard";
			Answers[19][1] = "Billy Gibbons";
			Answers[19][2] = "Billy Ethridge";
			Answers[19][3] = "Lainer Gregg";

			/*Init Music Correct Answers*/
			CorrectAnswer[0] = Answers[0][0];
			CorrectAnswer[1] = Answers[1][0];
			CorrectAnswer[2] = Answers[2][0];
			CorrectAnswer[3] = Answers[3][0];
			CorrectAnswer[4] = Answers[4][0];
			CorrectAnswer[5] = Answers[5][0];
			CorrectAnswer[6] = Answers[6][0];
			CorrectAnswer[7] = Answers[7][0];
			CorrectAnswer[8] = Answers[8][0];
			CorrectAnswer[9] = Answers[9][0];
			CorrectAnswer[10] = Answers[10][0];
			CorrectAnswer[11] = Answers[11][0];
			CorrectAnswer[12] = Answers[12][0];
			CorrectAnswer[13] = Answers[13][0];
			CorrectAnswer[14] = Answers[14][0];
			CorrectAnswer[15] = Answers[15][0];
			CorrectAnswer[16] = Answers[16][0];
			CorrectAnswer[17] = Answers[17][0];
			CorrectAnswer[18] = Answers[18][0];
			CorrectAnswer[19] = Answers[19][0];
		}

		if (set == 1)
		{
			/*Init Music Questions*/
			Questions[0] = "Which country has won Eurovision the most times?";
			Questions[1] = "What is the name of America's debut single?";
			Questions[2] = "'Let It Happen', 'The Less I Know the Better' and 'Elephant' are songs from which group?";
			Questions[3] = "Who was the first Eurovision winner for the UK?";
			Questions[4] = "What is the stage name of Anna Mae Bullock?";
			Questions[5] = "What song by PSY was the first YouTube video to be viewed 1 billion times?";
			Questions[6] = "What headphones company, co-founded by Dr. Dre, was purchased by Apple?";
			Questions[7] = "Which singer has the most Twitter followers?";
			Questions[8] = "Which band recorded the theme song to Friends?";
			Questions[9] = "Britney Spears and Justin Timberlake famously arrived at the American Music Awards in 2001 wearing what?";
			Questions[10] = "In 1984, Prince released his sixth studio album, entitled 'Purple Rain'. What was the album�s lead single?";
			Questions[11] = "What is the former lead singer of the rock band Marillion, Derek Dick, better known as?";
			Questions[12] = "What was Frank Sinatra�s middle name?";
			Questions[13] = "Right Said Fred had a No.1 Hit with 'I�m Too�'?";
			Questions[14] = "Which 1980's band are responsible for the hit 'Sweet Dreams (Are Made of This)'?";
			Questions[15] = "Bootylicious has become an accepted word in the Oxford Dictionary thanks to a track of the same name by who?";
			Questions[16] = "What was The Rolling Stones' second album called?";
			Questions[17] = "What was Barry Manilow's first No. 1 single?";
			Questions[18] = "What�s the name of Britney Spears� first single, released in 1998?";
			Questions[19] = "Which song gave Madonna her first UK number one?";

			/*Init Music Correct Answers*/
			Answers[0][0] = "Ireland";
			Answers[0][1] = "England";
			Answers[0][2] = "Russia";
			Answers[0][3] = "Poland";

			Answers[1][0] = "A Horse with No Name";
			Answers[1][1] = "America";
			Answers[1][2] = "Hearts";
			Answers[1][3] = "Star Banner";

			Answers[2][0] = "Tame Impala";
			Answers[2][1] = "The Who";
			Answers[2][2] = "The Angels ";
			Answers[2][3] = "Maroon 5";

			Answers[3][0] = "Sandie Shaw";
			Answers[3][1] = "Lulu";
			Answers[3][2] = "Katruna";
			Answers[3][3] = "John Adam";

			Answers[4][0] = "Tina Turner";
			Answers[4][1] = "AMB";
			Answers[4][2] = "Rihanna";
			Answers[4][3] = "Cindy Crawford";

			Answers[5][0] = "Gangnam Style";
			Answers[5][1] = "Gentleman";
			Answers[5][2] = "Daddy";
			Answers[5][3] = "Sexy Lady";

			Answers[6][0] = "Beats";
			Answers[6][1] = "JBL";
			Answers[6][2] = "Bose";
			Answers[6][3] = "Dre";

			Answers[7][0] = "Justin Bieber";
			Answers[7][1] = "Katie Perry";
			Answers[7][2] = "Rihanna";
			Answers[7][3] = "Beyonce";

			Answers[8][0] = "The Rembrandts";
			Answers[8][1] = "Barnenaked Ladies";
			Answers[8][2] = "Jess Frederick";
			Answers[8][3] = "AC/DC";

			Answers[9][0] = "Denim";
			Answers[9][1] = "Meat";
			Answers[9][2] = "Feather";
			Answers[9][3] = "Leather";

			Answers[10][0] = "When Doves Cry";
			Answers[10][1] = "Computer Blue";
			Answers[10][2] = "Baby I�m a Star";
			Answers[10][3] = "Purple Rain";

			Answers[11][0] = "Fish";
			Answers[11][1] = "Monkey";
			Answers[11][2] = "Cake";
			Answers[11][3] = "Weasel";

			Answers[12][0] = "Albert";
			Answers[12][1] = "Aissan ";
			Answers[12][2] = "South";
			Answers[12][3] = "Louis";

			Answers[13][0] = "Sexy";
			Answers[13][1] = "Into you";
			Answers[13][2] = "Boring";
			Answers[13][3] = "Loud";

			Answers[14][0] = "The Eurythmics";
			Answers[14][1] = "Guns N� Roses";
			Answers[14][2] = "Duran Duran";
			Answers[14][3] = "The Who";

			Answers[15][0] = "Destiny�s Child";
			Answers[15][1] = "Nicki Minaj";
			Answers[15][2] = "Sugar Babes";
			Answers[15][3] = "Spice Girls";

			Answers[16][0] = "The Rolling stones No.2";
			Answers[16][1] = "Sticky fingers";
			Answers[16][2] = "Let it Bleed";
			Answers[16][3] = "Some Girls";

			Answers[17][0] = "Mandy";
			Answers[17][1] = "Without You";
			Answers[17][2] = "Can't Smile";
			Answers[17][3] = "Home";

			Answers[18][0] = "Baby One more Time";
			Answers[18][1] = "Soda Pop";
			Answers[18][2] = "Toxic";
			Answers[18][3] = "Opps!...I Did It Again";

			Answers[19][0] = "Into the Groove";
			Answers[19][1] = "True Blue";
			Answers[19][2] = "La Isla Bonita";
			Answers[19][3] = "Vogue";

			/*Init Music Correct Answers*/
			CorrectAnswer[0] = Answers[0][0];
			CorrectAnswer[1] = Answers[1][0];
			CorrectAnswer[2] = Answers[2][0];
			CorrectAnswer[3] = Answers[3][0];
			CorrectAnswer[4] = Answers[4][0];
			CorrectAnswer[5] = Answers[5][0];
			CorrectAnswer[6] = Answers[6][0];
			CorrectAnswer[7] = Answers[7][0];
			CorrectAnswer[8] = Answers[8][0];
			CorrectAnswer[9] = Answers[9][0];
			CorrectAnswer[10] = Answers[10][0];
			CorrectAnswer[11] = Answers[11][0];
			CorrectAnswer[12] = Answers[12][0];
			CorrectAnswer[13] = Answers[13][0];
			CorrectAnswer[14] = Answers[14][0];
			CorrectAnswer[15] = Answers[15][0];
			CorrectAnswer[16] = Answers[16][0];
			CorrectAnswer[17] = Answers[17][0];
			CorrectAnswer[18] = Answers[18][0];
			CorrectAnswer[19] = Answers[19][0];
		}

		if (set == 2)
		{
			/*Init Music Questions*/
			Questions[0] = "Which American rock band had a hit in the eighties with the 'Eye of the Tiger'?";
			Questions[1] = "Who shot John Lennon outside his apartment in New York City on December 8 1980?";
			Questions[2] = "Who replaced Paul Di'Anno as lead singer of Iron Maiden?";
			Questions[3] = "What was the first album AC/DC released after the death of lead singer Bon Scott?";
			Questions[4] = "Daryl Hall and John Oates scored a number one single in 1984 with 'Out of ____'";
			Questions[5] = "Which Frankie Goes to Hollywood song was banned on UK radio play in 1984, because of its lyrics?";
			Questions[6] = "What musician was deported from Japan in the 1980�s for possession of marijuana?";
			Questions[7] = "Which famous music group were formerly known as the New Yardbirds?";
			Questions[8] = "Which Irish rock band sang The Boys Are Back In Town?";
			Questions[9] = "Eurovision sensation ABBA came from which country?";
			Questions[10] = "T Rex was led by which singer?";
			Questions[11] = "Whose first hit was Wuthering Heights?";
			Questions[12] = "What is the opening line to Queen's song 'Bohemian Rhapsody'?";
			Questions[13] = "Which Jamaican artist released a song in 1974 that begins with the lyrics, 'Everybody was Kung Fu fighting'?";
			Questions[14] = "Which member of Electric Light Orchestra left that band to form Wizzard in July 1972?";
			Questions[15] = "What was the name of David Bowie's alter ego?";
			Questions[16] = "Which Pink Floyd album featured the song 'Comfortably Numb'?";
			Questions[17] = "What year was the 'Y.M.C.A.' released?";
			Questions[18] = "Devil Woman was a hit for Cliff Richard in what year?";
			Questions[19] = "'Every Picture Tells a Story' was the title of an album released by which artist in 1971?";

			/*Init Music Correct Answers*/
			Answers[0][0] = "Survivor";
			Answers[0][1] = "Foreigner";
			Answers[0][2] = "Honeymoon Suite";
			Answers[0][3] = "Renegade";

			Answers[1][0] = "Mark Chapman";
			Answers[1][1] = "Frankie Bobard";
			Answers[1][2] = "John Oates";
			Answers[1][3] = "Jacob Lennon";

			Answers[2][0] = "Bruce Dickson";
			Answers[2][1] = "Steve Harris";
			Answers[2][2] = "Dave Murray";
			Answers[2][3] = "Shane Malon";

			Answers[3][0] = "Black in Black";
			Answers[3][1] = "High Voltage";
			Answers[3][2] = "Live at River Plate";
			Answers[3][3] = "Hell�s Maw";

			Answers[4][0] = "Touch";
			Answers[4][1] = "Light";
			Answers[4][2] = "Distance";
			Answers[4][3] = "Sight";

			Answers[5][0] = "Relax";
			Answers[5][1] = "Rage Hard";
			Answers[5][2] = "Krisco Kisses";
			Answers[5][3] = "Sensations";

			Answers[6][0] = "Paul McCartney";
			Answers[6][1] = "George Michael";
			Answers[6][2] = "David Bowie";
			Answers[6][3] = "Elton John";

			Answers[7][0] = "Led Zeppelin";
			Answers[7][1] = "Telge Blues";
			Answers[7][2] = "Band of Joy";
			Answers[7][3] = "The Who";

			Answers[8][0] = "Thin Lizzy";
			Answers[8][1] = "The Cranberries";
			Answers[8][2] = "Horslips";
			Answers[8][3] = "Calops";

			Answers[9][0] = "Sweden";
			Answers[9][1] = "United Kingdom";
			Answers[9][2] = "Ireland";
			Answers[9][3] = "Switzerland";

			Answers[10][0] = "Marc Bolan";
			Answers[10][1] = "Mickey Finn";
			Answers[10][2] = "Gloria Jones";
			Answers[10][3] = "Tony Rex";

			Answers[11][0] = "Kate Bush";
			Answers[11][1] = "Andrew Powell";
			Answers[11][2] = "Heathcliff";
			Answers[11][3] = "Sugar Babes";

			Answers[12][0] = "Is This The Real Life?";
			Answers[12][1] = "Too Late, My Time Has Come";
			Answers[12][2] = "Mamaaa";
			Answers[12][3] = "Is This Just Fantasy";

			Answers[13][0] = "Carl Douglas";
			Answers[13][1] = "Bob Marley";
			Answers[13][2] = "Carlton Hylton";
			Answers[13][3] = "Peter Tosh";

			Answers[14][0] = "Roy Wood";
			Answers[14][1] = "Jeff Lynne";
			Answers[14][2] = "Rodie Vela";
			Answers[14][3] = "Richard Tany";

			Answers[15][0] = "Ziggy Stardust";
			Answers[15][1] = "Linon";
			Answers[15][2] = "Space Oddity";
			Answers[15][3] = "Fizzle Pop";

			Answers[16][0] = "The Walls";
			Answers[16][1] = "Wish You Were Here";
			Answers[16][2] = "Animals";
			Answers[16][3] = "Pulse";

			Answers[17][0] = "1978";
			Answers[17][1] = "1980";
			Answers[17][2] = "1977";
			Answers[17][3] = "1982";

			Answers[18][0] = "1976";
			Answers[18][1] = "1975";
			Answers[18][2] = "1978";
			Answers[18][3] = "1977";

			Answers[19][0] = "Rod Steward";
			Answers[19][1] = "Paul McCartney";
			Answers[19][2] = "The Who";
			Answers[19][3] = "John Lennon";

			/*Init Music Correct Answers*/
			CorrectAnswer[0] = Answers[0][0];
			CorrectAnswer[1] = Answers[1][0];
			CorrectAnswer[2] = Answers[2][0];
			CorrectAnswer[3] = Answers[3][0];
			CorrectAnswer[4] = Answers[4][0];
			CorrectAnswer[5] = Answers[5][0];
			CorrectAnswer[6] = Answers[6][0];
			CorrectAnswer[7] = Answers[7][0];
			CorrectAnswer[8] = Answers[8][0];
			CorrectAnswer[9] = Answers[9][0];
			CorrectAnswer[10] = Answers[10][0];
			CorrectAnswer[11] = Answers[11][0];
			CorrectAnswer[12] = Answers[12][0];
			CorrectAnswer[13] = Answers[13][0];
			CorrectAnswer[14] = Answers[14][0];
			CorrectAnswer[15] = Answers[15][0];
			CorrectAnswer[16] = Answers[16][0];
			CorrectAnswer[17] = Answers[17][0];
			CorrectAnswer[18] = Answers[18][0];
			CorrectAnswer[19] = Answers[19][0];
		}
	}

	if (subject == Subjects[5])
	{
		if (set == 0)
		{
			/*Init Toys & Games Questions*/
			Questions[0] = "The most played Game in the world currently?";
			Questions[1] = "What is the best selling game of all time?";
			Questions[2] = "Which country is the home of Lego?";
			Questions[3] = "How many pawns start in a game of chess?";
			Questions[4] = "What video game series features Lara Croft?";
			Questions[5] = "How many feet of wire does it take to make one Slinky?";
			Questions[6] = "What doll, created in 1959, was named after the inventor's daughter?";
			Questions[7] = "What is the bestselling Video Game Console?";
			Questions[8] = "What is the bestselling game franchise?";
			Questions[9] = "What is the bestselling Board game?";
			Questions[10] = "What year was the Super Nintendo Entertainment System (SNES) released?";
			Questions[11] = "What year was Nintendo founded?";
			Questions[12] = "In which game must you shout its name when you have one card remaining?";
			Questions[13] = "Blizzard Entertainment is most well-known for what video game franchise?";
			Questions[14] = "Nintendo owns two of the most popular gaming franchises ever. What are they?";
			Questions[15] = "What is the highest-grossing movie based on a video game?";
			Questions[16] = "How many maximum controllers are supported by PlayStation 3 system?";
			Questions[17] = "What is the name of the final course in most Mario kart games?";
			Questions[18] = "Solid Snake is a hero in which Franchise?";
			Questions[19] = "Fight Night 2004 is about which sport?";

			/*Init Toys & Games Correct Answers*/
			Answers[0][0] = "Crossfire";
			Answers[0][1] = "Fortnite";
			Answers[0][2] = "PUBG";
			Answers[0][3] = "Call of Duty: Warzone";

			Answers[1][0] = "Minecraft";
			Answers[1][1] = "Tetris";
			Answers[1][2] = "GTA 5";
			Answers[1][3] = "Wii Sports";

			Answers[2][0] = "Denmark";
			Answers[2][1] = "Sweden";
			Answers[2][2] = "Switzerland";
			Answers[2][3] = "USA";

			Answers[3][0] = "16";
			Answers[3][1] = "10";
			Answers[3][2] = "12";
			Answers[3][3] = "8";

			Answers[4][0] = "Tomb Raider";
			Answers[4][1] = "Myst";
			Answers[4][2] = "Street Fighter";
			Answers[4][3] = "Raiders";

			Answers[5][0] = "80ft";
			Answers[5][1] = "20ft";
			Answers[5][2] = "100ft";
			Answers[5][3] = "40ft";

			Answers[6][0] = "Barbie";
			Answers[6][1] = "Raggedy Ann";
			Answers[6][2] = "Winnie the Pooh";
			Answers[6][3] = "Strawberry Shortcake";

			Answers[7][0] = "PlayStation 2";
			Answers[7][1] = "PlayStation 4";
			Answers[7][2] = "PlayStation";
			Answers[7][3] = "Xbox 360";

			Answers[8][0] = "Mario";
			Answers[8][1] = "Pok�mon";
			Answers[8][2] = "FIFA";
			Answers[8][3] = "Grand Theft Auto";

			Answers[9][0] = "Chess";
			Answers[9][1] = "Checkers";
			Answers[9][2] = "Monopoly";
			Answers[9][3] = "Scrabble";

			Answers[10][0] = "1991";
			Answers[10][1] = "1989";
			Answers[10][2] = "1999";
			Answers[10][3] = "1993";

			Answers[11][0] = "1889";
			Answers[11][1] = "1979";
			Answers[11][2] = "1885";
			Answers[11][3] = "1991";

			Answers[12][0] = "Uno";
			Answers[12][1] = "Yahtzee";
			Answers[12][2] = "Snap";
			Answers[12][3] = "Fish";

			Answers[13][0] = "World of Warcraft";
			Answers[13][1] = "Fortnite";
			Answers[13][2] = "League of Legends";
			Answers[13][3] = "DOTA";

			Answers[14][0] = "Mario and Pok�mon";
			Answers[14][1] = "Mario and Sonic";
			Answers[14][2] = "Mario and Luigi";
			Answers[14][3] = "Mario and Super Mario";

			Answers[15][0] = "Detective Pikachu";
			Answers[15][1] = "Assassin�s Creed";
			Answers[15][2] = "Doom";
			Answers[15][3] = "Resident Evil";

			Answers[16][0] = "7";
			Answers[16][1] = "6";
			Answers[16][2] = "5";
			Answers[16][3] = "10";

			Answers[17][0] = "Rainbow Road";
			Answers[17][1] = "Victory Lane";
			Answers[17][2] = "Highway to Freedom";
			Answers[17][3] = "Rainbow Alley";

			Answers[18][0] = "Metal Gear";
			Answers[18][1] = "Solid Gear";
			Answers[18][2] = "Gear Solid";
			Answers[18][3] = "Stealth";

			Answers[19][0] = "Boxing";
			Answers[19][1] = "UFC";
			Answers[19][2] = "MMA";
			Answers[19][3] = "Cage Fighting";

			/*Init Toys & Games Correct Answers*/
			CorrectAnswer[0] = Answers[0][0];
			CorrectAnswer[1] = Answers[1][0];
			CorrectAnswer[2] = Answers[2][0];
			CorrectAnswer[3] = Answers[3][0];
			CorrectAnswer[4] = Answers[4][0];
			CorrectAnswer[5] = Answers[5][0];
			CorrectAnswer[6] = Answers[6][0];
			CorrectAnswer[7] = Answers[7][0];
			CorrectAnswer[8] = Answers[8][0];
			CorrectAnswer[9] = Answers[9][0];
			CorrectAnswer[10] = Answers[10][0];
			CorrectAnswer[11] = Answers[11][0];
			CorrectAnswer[12] = Answers[12][0];
			CorrectAnswer[13] = Answers[13][0];
			CorrectAnswer[14] = Answers[14][0];
			CorrectAnswer[15] = Answers[15][0];
			CorrectAnswer[16] = Answers[16][0];
			CorrectAnswer[17] = Answers[17][0];
			CorrectAnswer[18] = Answers[18][0];
			CorrectAnswer[19] = Answers[19][0];
		}

		if (set == 1)
		{
			/*Init Toys & Games Questions*/
			Questions[0] = "What game is generally considered to have ushered in the video game era?";
			Questions[1] = "Play-Doh was originally invented for what purpose?";
			Questions[2] = "What toy, first introduced at the Nuremberg Toy Show in 1979, sold over 100 million units by 1982?";
			Questions[3] = "What is the longest Running Video Game series?";
			Questions[4] = "In Backgammon, how many pieces does each player start with?";
			Questions[5] = "How many different Scrabble tiles are there?";
			Questions[6] = "From which TV show, was created a number of very popular toys and also the ice lollies Zoom and Fab?";
			Questions[7] = "What year was the first virtual reality headset created?";
			Questions[8] = "What time was L.A. Noire modelled after?";
			Questions[9] = "What is the original First-Person Shooter game?";
			Questions[10] = "Which of these in Dungeons and Dragons is considered a medium character?";
			Questions[11] = "Which of the following card games is not about getting rid of all your cards?";
			Questions[12] = "Where was Bingo first played?";
			Questions[13] = "Which country banned the sale of the original Grand Theft Auto?";
			Questions[14] = "Which famous entertainer composed music for Sonic 3?";
			Questions[15] = "How long did it take Markus Persson to create the first version of Minecraft?";
			Questions[16] = "What is the highest level a player can reach in Pac-Man?";
			Questions[17] = "Which of the following is the only game from the 1970�s to generate over one billion US dollars in revenue?";
			Questions[18] = "Which famous video game character started at as nothing but a placeholder?";
			Questions[19] = "The classic Atari 2600 game controller has how many buttons?";

			/*Init Toys & Games Correct Answers*/
			Answers[0][0] = "Pong";
			Answers[0][1] = "Pac-Man";
			Answers[0][2] = "Space Invaders";
			Answers[0][3] = "Asteroids";

			Answers[1][0] = "Cleaning Wallpaper";
			Answers[1][1] = "Sealing Fruit";
			Answers[1][2] = "Sculpting";
			Answers[1][3] = "Replace Tac";

			Answers[2][0] = "Rubik�s Cube";
			Answers[2][1] = "My Little Pony";
			Answers[2][2] = "Pictionary";
			Answers[2][3] = "Cabbage Patch Kids";

			Answers[3][0] = "Pac-man";
			Answers[3][1] = "Mario";
			Answers[3][2] = "Space Invaders";
			Answers[3][3] = "Galaxian";

			Answers[4][0] = "15";
			Answers[4][1] = "16";
			Answers[4][2] = "18";
			Answers[4][3] = "13";

			Answers[5][0] = "27";
			Answers[5][1] = "26";
			Answers[5][2] = "30";
			Answers[5][3] = "25";

			Answers[6][0] = "Thunderbirds";
			Answers[6][1] = "Transformers";
			Answers[6][2] = "G.I Joe";
			Answers[6][3] = "He-Man";

			Answers[7][0] = "1995";
			Answers[7][1] = "2005";
			Answers[7][2] = "2002";
			Answers[7][3] = "1999";

			Answers[8][0] = "1940�s";
			Answers[8][1] = "1950�s";
			Answers[8][2] = "1930�s";
			Answers[8][3] = "1960�s";

			Answers[9][0] = "Wolfenstein 3D";
			Answers[9][1] = "Wolfenstein";
			Answers[9][2] = "Doom";
			Answers[9][3] = "Call of Duty";

			Answers[10][0] = "Dwarf";
			Answers[10][1] = "Gnome";
			Answers[10][2] = "Halfling";
			Answers[10][3] = "Elf";

			Answers[11][0] = "Gin rummy";
			Answers[11][1] = "Crazy eights";
			Answers[11][2] = "Uno";
			Answers[11][3] = "Bullshit";

			Answers[12][0] = "Italy";
			Answers[12][1] = "UK";
			Answers[12][2] = "India";
			Answers[12][3] = "China";

			Answers[13][0] = "Brazil";
			Answers[13][1] = "USA";
			Answers[13][2] = "Peru";
			Answers[13][3] = "Mexico";

			Answers[14][0] = "Michael Jackson";
			Answers[14][1] = "Jay Z";
			Answers[14][2] = "Elton John";
			Answers[14][3] = "George Michael";

			Answers[15][0] = "One Week";
			Answers[15][1] = "One Year";
			Answers[15][2] = "Three Months";
			Answers[15][3] = "Three Weeks";

			Answers[16][0] = "256";
			Answers[16][1] = "1024";
			Answers[16][2] = "4096";
			Answers[16][3] = "512";

			Answers[17][0] = "Space Invaders";
			Answers[17][1] = "Pac-Man";
			Answers[17][2] = "Tetris";
			Answers[17][3] = "Pong";

			Answers[18][0] = "Kirby";
			Answers[18][1] = "Pac-Man";
			Answers[18][2] = "Mario";
			Answers[18][3] = "Sonic";

			Answers[19][0] = "1";
			Answers[19][1] = "3";
			Answers[19][2] = "2";
			Answers[19][3] = "5";

			/*Init Toys & Games Correct Answers*/
			CorrectAnswer[0] = Answers[0][0];
			CorrectAnswer[1] = Answers[1][0];
			CorrectAnswer[2] = Answers[2][0];
			CorrectAnswer[3] = Answers[3][0];
			CorrectAnswer[4] = Answers[4][0];
			CorrectAnswer[5] = Answers[5][0];
			CorrectAnswer[6] = Answers[6][0];
			CorrectAnswer[7] = Answers[7][0];
			CorrectAnswer[8] = Answers[8][0];
			CorrectAnswer[9] = Answers[9][0];
			CorrectAnswer[10] = Answers[10][0];
			CorrectAnswer[11] = Answers[11][0];
			CorrectAnswer[12] = Answers[12][0];
			CorrectAnswer[13] = Answers[13][0];
			CorrectAnswer[14] = Answers[14][0];
			CorrectAnswer[15] = Answers[15][0];
			CorrectAnswer[16] = Answers[16][0];
			CorrectAnswer[17] = Answers[17][0];
			CorrectAnswer[18] = Answers[18][0];
			CorrectAnswer[19] = Answers[19][0];
		}

		if (set == 2)
		{
			/*Init Toys & Games Questions*/
			Questions[0] = "What is the only king in a ordinary deck of cards without a moustache?";
			Questions[1] = "What toy is created by mixing silicone oil and boric acid?";
			Questions[2] = "What popular action figure is named after a pigeon?";
			Questions[3] = "The oldest board game is?";
			Questions[4] = "Which of these is not a token to play as in the original Monopoly?";
			Questions[5] = "The United States Air Force used what gaming console to create a cluster supercomputer?";
			Questions[6] = "English musician Anthony Ernest Pratt was the inventor of which board game?";
			Questions[7] = "What is the name of the first-ever videogame?";
			Questions[8] = "Nintendo began as a company selling which products?";
			Questions[9] = "What popular videogame character goes by the codename �Killer Bee�?";
			Questions[10] = "What is the name of the woman Mario is attempting to save from the giant ape in the original Donkey Kong?";
			Questions[11] = "What is the name of the legendary videogame designer who created the Mario franchise?";
			Questions[12] = "Which Mario character has appeared in all but one Mario Party game?";
			Questions[13] = "In which star system does the game Star Fox take place?";
			Questions[14] = "The prototype of the PlayStation was designed in collaboration with which company?";
			Questions[15] = "Three of Pac-Man�s ghost enemies are named Blinky, Pinky and Inky. What is the name of the fourth member?";
			Questions[16] = "What type of animal is Sonic's companion, Knuckles?";
			Questions[17] = "What does the game rating system used across Europe and Asia, PEGI, stand for?";
			Questions[18] = "What is the name of the alien race who were the adoptive parents of Samus in the Metroid franchise?";
			Questions[19] = "How many different shapes are there in Tetris?";

			/*Init Toys & Games Correct Answers*/
			Answers[0][0] = "King of Diamonds";
			Answers[0][1] = "King of Clubs";
			Answers[0][2] = "King of Spades";
			Answers[0][3] = "King of Hearts";

			Answers[1][0] = "Silly Putty";
			Answers[1][1] = "Play-Doh";
			Answers[1][2] = "Finger Paints";
			Answers[1][3] = "Crayons";

			Answers[2][0] = "G.I. Joe";
			Answers[2][1] = "Action Man";
			Answers[2][2] = "He-Man";
			Answers[2][3] = "Power Rangers";

			Answers[3][0] = "Senet";
			Answers[3][1] = "Chess";
			Answers[3][2] = "Backgammon";
			Answers[3][3] = "Checkers";

			Answers[4][0] = "Steam Ship";
			Answers[4][1] = "Iron";
			Answers[4][2] = "Top Hat";
			Answers[4][3] = "Boot";

			Answers[5][0] = "PlayStation 3";
			Answers[5][1] = "PlayStation";
			Answers[5][2] = "Xbox One";
			Answers[5][3] = "Atari";

			Answers[6][0] = "Cluedo";
			Answers[6][1] = "Yahtzee";
			Answers[6][2] = "Monopoly";
			Answers[6][3] = "Checkers";

			Answers[7][0] = "Tennis for Two";
			Answers[7][1] = "Pong";
			Answers[7][2] = "Space Race";
			Answers[7][3] = "Tetris";

			Answers[8][0] = "Playing Cards";
			Answers[8][1] = "Action Figures";
			Answers[8][2] = "Board Games";
			Answers[8][3] = "Soap";

			Answers[9][0] = "Cammy, Street Fighter";
			Answers[9][1] = "Scorpion, Mortal Kombat";
			Answers[9][2] = "Lee, Tekken";
			Answers[9][3] = "Li Long, Soulcalibur";

			Answers[10][0] = "Pauline";
			Answers[10][1] = "Princess Peach";
			Answers[10][2] = "Peach";
			Answers[10][3] = "Rosalina";

			Answers[11][0] = "Shigeru Miyamoto";
			Answers[11][1] = "Hideo Kojima";
			Answers[11][2] = "Hironobu Sakaguchi";
			Answers[11][3] = "Shinji Mikami";

			Answers[12][0] = "Wario";
			Answers[12][1] = "Mario";
			Answers[12][2] = "Luigi";
			Answers[12][3] = "Donkey Kong";

			Answers[13][0] = "Lylat ";
			Answers[13][1] = "Foxway";
			Answers[13][2] = "Centura ";
			Answers[13][3] = "Alastria";

			Answers[14][0] = "Nintendo";
			Answers[14][1] = "SEGA";
			Answers[14][2] = "Atari";
			Answers[14][3] = "Hasbro";

			Answers[15][0] = "Pokey";
			Answers[15][1] = "Hokey";
			Answers[15][2] = "Jokey";
			Answers[15][3] = "Dokey";

			Answers[16][0] = "Echidna";
			Answers[16][1] = "Hedgehog";
			Answers[16][2] = "Fox";
			Answers[16][3] = "Moonrat";

			Answers[17][0] = "Pan European Game Information";
			Answers[17][1] = "Pan Eurasian Game Identifier";
			Answers[17][2] = "Pan European Game Identifier";
			Answers[17][3] = "Pan Eurasian Game Information";

			Answers[18][0] = "The Chozo";
			Answers[18][1] = "The Metroid";
			Answers[18][2] = "The Phazon";
			Answers[18][3] = "The Beetom";

			Answers[19][0] = "7";
			Answers[19][1] = "10";
			Answers[19][2] = "12";
			Answers[19][3] = "9";

			/*Init Toys & Games Correct Answers*/
			CorrectAnswer[0] = Answers[0][0];
			CorrectAnswer[1] = Answers[1][0];
			CorrectAnswer[2] = Answers[2][0];
			CorrectAnswer[3] = Answers[3][0];
			CorrectAnswer[4] = Answers[4][0];
			CorrectAnswer[5] = Answers[5][0];
			CorrectAnswer[6] = Answers[6][0];
			CorrectAnswer[7] = Answers[7][0];
			CorrectAnswer[8] = Answers[8][0];
			CorrectAnswer[9] = Answers[9][0];
			CorrectAnswer[10] = Answers[10][0];
			CorrectAnswer[11] = Answers[11][0];
			CorrectAnswer[12] = Answers[12][0];
			CorrectAnswer[13] = Answers[13][0];
			CorrectAnswer[14] = Answers[14][0];
			CorrectAnswer[15] = Answers[15][0];
			CorrectAnswer[16] = Answers[16][0];
			CorrectAnswer[17] = Answers[17][0];
			CorrectAnswer[18] = Answers[18][0];
			CorrectAnswer[19] = Answers[19][0];
		}
	}

	if (subject == Subjects[6])
	{
		if (set == 0)
		{
			/*Init Geography Questions*/
			Questions[0] = "Which of these U.S. states does NOT border Canada?";
			Questions[1] = "Which of these countries was NOT a part of the Soviet Union?";
			Questions[2] = "Which of these cities is NOT a national capital?";
			Questions[3] = "Which of these cities DOES NOT border the Mediterranean Sea?";
			Questions[4] = " Which of these countries was NEVER part of the British Empire?";
			Questions[5] = "Which one of these cities is NOT in the Southern Hemisphere?";
			Questions[6] = "Which one of these countries is NOT in Central America?";
			Questions[7] = "Which of these cities does NOT border the Great Lakes?";
			Questions[8] = "Which of these countries is NOT majority-Muslim?";
			Questions[9] = "Which of these countries is NOT recognized by the United Nations?";
			Questions[10] = "Which of these mountain ranges is NOT in Europe?";
			Questions[11] = "Which of these things is NOT located in Africa?";
			Questions[12] = "Which of these countries is NOT one of the top 20 oil producers?";
			Questions[13] = "Which of these cities is NOT in India?";
			Questions[14] = "Which of these countries does NOT have only one border?";
			Questions[15] = "Which of these cities has the most air pollution?";
			Questions[16] = "Which of these countries borders the most other countries?";
			Questions[17] = "Which of these countries has the highest percentage of Catholics?";
			Questions[18] = "Which of these countries has the highest GDP per capita?";
			Questions[19] = "Which of these countries has the highest fertility rate?";

			/*Init Geography Correct Answers*/
			Answers[0][0] = "Indiana";
			Answers[0][1] = "Minnesota";
			Answers[0][2] = "Alaska";
			Answers[0][3] = "Maine";

			Answers[1][0] = "Poland";
			Answers[1][1] = "Belarus";
			Answers[1][2] = "Ukraine";
			Answers[1][3] = "Georgia";

			Answers[2][0] = "Sydney";
			Answers[2][1] = "Cairo";
			Answers[2][2] = "Prague";
			Answers[2][3] = "Bangkok";

			Answers[3][0] = "Lisbon";
			Answers[3][1] = "Alexandria";
			Answers[3][2] = "Barcelona";
			Answers[3][3] = "Monaco";

			Answers[4][0] = "Thailand";
			Answers[4][1] = "Kenya";
			Answers[4][2] = "New Zealand";
			Answers[4][3] = "Ireland";

			Answers[5][0] = "Colombo";
			Answers[5][1] = "Brisbane";
			Answers[5][2] = "Brasilia";
			Answers[5][3] = "Johannesburg";

			Answers[6][0] = "Suriname";
			Answers[6][1] = "Honduras";
			Answers[6][2] = "Belize";
			Answers[6][3] = "Panama";

			Answers[7][0] = "Pittsburgh";
			Answers[7][1] = "Toronto";
			Answers[7][2] = "Chicago";
			Answers[7][3] = "Cleveland";

			Answers[8][0] = "Ethiopia";
			Answers[8][1] = "Indonesia";
			Answers[8][2] = "Albania";
			Answers[8][3] = "Morocco";

			Answers[9][0] = "Taiwan";
			Answers[9][1] = "Iran";
			Answers[9][2] = "Israel";
			Answers[9][3] = "Cyprus";

			Answers[10][0] = "Atlas Mountains";
			Answers[10][1] = "The Alps";
			Answers[10][2] = "The Pyrenees";
			Answers[10][3] = "Carpathian Mountains";

			Answers[11][0] = "Gobi desert";
			Answers[11][1] = "Aswan Dam";
			Answers[11][2] = "Lake Victoria";
			Answers[11][3] = "Zambezi River";

			Answers[12][0] = "Morocco";
			Answers[12][1] = "Norway";
			Answers[12][2] = "Iraq";
			Answers[12][3] = "United States";

			Answers[13][0] = "Karachi";
			Answers[13][1] = "Chennai";
			Answers[13][2] = "Bangalore";
			Answers[13][3] = "Bhopal";

			Answers[14][0] = "North Korea";
			Answers[14][1] = "Haiti";
			Answers[14][2] = "Portugal";
			Answers[14][3] = "Ireland";

			Answers[15][0] = "Delhi";
			Answers[15][1] = "Los Angeles";
			Answers[15][2] = "Tokyo";
			Answers[15][3] = "Rome";

			Answers[16][0] = "Germany";
			Answers[16][1] = "United States";
			Answers[16][2] = "Egypt";
			Answers[16][3] = "Vietnam";

			Answers[17][0] = "Poland";
			Answers[17][1] = "Cuba";
			Answers[17][2] = "Germany";
			Answers[17][3] = "South Korea";

			Answers[18][0] = "Switzerland";
			Answers[18][1] = "Egypt";
			Answers[18][2] = "Spain";
			Answers[18][3] = "China";



			Answers[19][0] = "Tanzania";
			Answers[19][1] = "Canada";
			Answers[19][2] = "India";
			Answers[19][3] = "Mexico";

			/*Init Geography Correct Answers*/
			CorrectAnswer[0] = Answers[0][0];
			CorrectAnswer[1] = Answers[1][0];
			CorrectAnswer[2] = Answers[2][0];
			CorrectAnswer[3] = Answers[3][0];
			CorrectAnswer[4] = Answers[4][0];
			CorrectAnswer[5] = Answers[5][0];
			CorrectAnswer[6] = Answers[6][0];
			CorrectAnswer[7] = Answers[7][0];
			CorrectAnswer[8] = Answers[8][0];
			CorrectAnswer[9] = Answers[9][0];
			CorrectAnswer[10] = Answers[10][0];
			CorrectAnswer[11] = Answers[11][0];
			CorrectAnswer[12] = Answers[12][0];
			CorrectAnswer[13] = Answers[13][0];
			CorrectAnswer[14] = Answers[14][0];
			CorrectAnswer[15] = Answers[15][0];
			CorrectAnswer[16] = Answers[16][0];
			CorrectAnswer[17] = Answers[17][0];
			CorrectAnswer[18] = Answers[18][0];
			CorrectAnswer[19] = Answers[19][0];
		}

		if (set == 1)
		{
			/*Init Geography Questions*/
			Questions[0] = "Which of these countries borders the Mediterranean Sea?";
			Questions[1] = "Which of these mountains is located in the United States?";
			Questions[2] = "Which of these countries is in Africa?";
			Questions[3] = "Which of these seas borders Italy?";
			Questions[4] = "Which of these is an official language of South Africa?";
			Questions[5] = "Which of these currencies is still in use today?";
			Questions[6] = "Which of these European capitals is near the coast?";
			Questions[7] = "Which of these countries is located in Indochina?";
			Questions[8] = "Which of these countries is on the Equator?";
			Questions[9] = "Which of these countries is part of the European Union?";
			Questions[10] = "Which of these countries has a capital city whose name starts with C?";
			Questions[11] = "Which of these countries has a shrinking population?";
			Questions[12] = "Which of these countries has a king?";
			Questions[13] = "Which of these countries was uninhabited in the year 1 AD?";
			Questions[14] = "Which of these cities is in Scotland?";
			Questions[15] = "Which of these countries has the highest life expectancy?";
			Questions[16] = "Which of these rivers flows past the most countries?";
			Questions[17] = "Which of these cities has the highest population?";
			Questions[18] = "Which of these islands has the largest area?";
			Questions[19] = "Which of these capital cities gets the most rain?";

			/*Init Geography Correct Answers*/
			Answers[0][0] = "Croatia";
			Answers[0][1] = "Germany";
			Answers[0][2] = "Andorra";
			Answers[0][3] = "Saudi Arabia";

			Answers[1][0] = "Mount rainier";
			Answers[1][1] = "Mount Etna";
			Answers[1][2] = "Annapurna";
			Answers[1][3] = "Mont Blanc";

			Answers[2][0] = "Sudan";
			Answers[2][1] = "Bhutan";
			Answers[2][2] = "Portugal";
			Answers[2][3] = "Ecuador";

			Answers[3][0] = "Adriatic Sea";
			Answers[3][1] = "Black Sea";
			Answers[3][2] = "Caspian Sea";
			Answers[3][3] = "Red Sea";

			Answers[4][0] = "Xhosa";
			Answers[4][1] = "Arabic";
			Answers[4][2] = "French";
			Answers[4][3] = "Mandrain";

			Answers[5][0] = "Swiss Franc";
			Answers[5][1] = "Deutsche Mark";
			Answers[5][2] = "Italian Lira";
			Answers[5][3] = "Spanish Peseta";

			Answers[6][0] = "Dublin";
			Answers[6][1] = "Paris";
			Answers[6][2] = "Vienna";
			Answers[6][3] = "Madrid";

			Answers[7][0] = "Cambodia";
			Answers[7][1] = "Sweden";
			Answers[7][2] = "China";
			Answers[7][3] = "Uruguay";

			Answers[8][0] = "Democratic Republic Of the congo";
			Answers[8][1] = "Australia";
			Answers[8][2] = "Egypt";
			Answers[8][3] = "Kenya";

			Answers[9][0] = "Poland";
			Answers[9][1] = "Turkey";
			Answers[9][2] = "Georgia";
			Answers[9][3] = "Switzerland";

			Answers[10][0] = "Australia";
			Answers[10][1] = "Sweden";
			Answers[10][2] = "China";
			Answers[10][3] = "Uruguay";

			Answers[11][0] = "Romania";
			Answers[11][1] = "United States";
			Answers[11][2] = "Pakistan";
			Answers[11][3] = "Kenya";

			Answers[12][0] = "Jordan";
			Answers[12][1] = "Egypt";
			Answers[12][2] = "Mexico";
			Answers[12][3] = "Italy";

			Answers[13][0] = "Iceland";
			Answers[13][1] = "Venezuela";
			Answers[13][2] = "Kenya";
			Answers[13][3] = "Australia";

			Answers[14][0] = "Aberdeen";
			Answers[14][1] = "Cardiff";
			Answers[14][2] = "York";
			Answers[14][3] = "Bristol";

			Answers[15][0] = "Japan";
			Answers[15][1] = "UK";
			Answers[15][2] = "Philippines";
			Answers[15][3] = "Argentina";

			Answers[16][0] = "Danube";
			Answers[16][1] = "Euphrates";
			Answers[16][2] = "Mississippi";
			Answers[16][3] = "Amazon";

			Answers[17][0] = "Shanghai";
			Answers[17][1] = "Singapore";
			Answers[17][2] = "Santiago";
			Answers[17][3] = "San Francisco";

			Answers[18][0] = "Madagascar";
			Answers[18][1] = "UK";
			Answers[18][2] = "Iceland";
			Answers[18][3] = "Cuba";

			Answers[19][0] = "Kuala Lumpur";
			Answers[19][1] = "Wellington";
			Answers[19][2] = "Mexico City";
			Answers[19][3] = "Washington D.C";

			/*Init Geography Correct Answers*/
			CorrectAnswer[0] = Answers[0][0];
			CorrectAnswer[1] = Answers[1][0];
			CorrectAnswer[2] = Answers[2][0];
			CorrectAnswer[3] = Answers[3][0];
			CorrectAnswer[4] = Answers[4][0];
			CorrectAnswer[5] = Answers[5][0];
			CorrectAnswer[6] = Answers[6][0];
			CorrectAnswer[7] = Answers[7][0];
			CorrectAnswer[8] = Answers[8][0];
			CorrectAnswer[9] = Answers[9][0];
			CorrectAnswer[10] = Answers[10][0];
			CorrectAnswer[11] = Answers[11][0];
			CorrectAnswer[12] = Answers[12][0];
			CorrectAnswer[13] = Answers[13][0];
			CorrectAnswer[14] = Answers[14][0];
			CorrectAnswer[15] = Answers[15][0];
			CorrectAnswer[16] = Answers[16][0];
			CorrectAnswer[17] = Answers[17][0];
			CorrectAnswer[18] = Answers[18][0];
			CorrectAnswer[19] = Answers[19][0];
		}

		if (set == 2)
		{
			/*Init Geography Questions*/
			Questions[0] = "If you dug directly through the centre of the Earth from London, where would you emerge?";
			Questions[1] = "Which of the following is a nickname of Aberdeen, Scotland?";
			Questions[2] = "Which is the largest continent (by area)?";
			Questions[3] = "Roughly the equatorial circumference of Earth?";
			Questions[4] = "How many countries sit along the equator?";
			Questions[5] = "In which city, is the Big Nickel located in Canada?";
			Questions[6] = "What year is on the flag of the US state Wisconsin?";
			Questions[7] = "How many countries border Kyrgyzstan ";
			Questions[8] = "What is the currency of Switzerland?";
			Questions[9] = "What language do most Brazilians speak?";
			Questions[10] = "UNESCO protects the world�s most important sites. How many World Heritage Sites have they inscribed?";
			Questions[11] = "In Iceland, you can walk across two tectonic plates, side by side. Which ones, exactly?";
			Questions[12] = "Do you know the name (and location) of the world�s deepest lake?";
			Questions[13] = "Which river flows through Paris?";
			Questions[14] = "The Seychelles are located in which ocean?";
			Questions[15] = "The region of Siberia occupies approximately three quarters of which country?";
			Questions[16] = "Mount Vesuvius overlooks which modern Italian city?";
			Questions[17] = "Which English city has more miles of canals than Venice?";
			Questions[18] = "Before the Euro, what was the currency of Greece?";
			Questions[19] = "Pho and banh mi are dishes from which country?";

			/*Init Geography Correct Answers*/
			Answers[0][0] = "Granite City";
			Answers[0][1] = "Cottonopolis";
			Answers[0][2] = "The Venice of the North";
			Answers[0][3] = "The Big Smoke";

			Answers[1][0] = "Asia";
			Answers[1][1] = "Antarctica";
			Answers[1][2] = "North America";
			Answers[1][3] = "Africa";

			Answers[2][0] = "40,000 km";
			Answers[2][1] = "15,000 km";
			Answers[2][2] = "70,000 km";
			Answers[2][3] = "55,000 km";

			Answers[3][0] = "13";
			Answers[3][1] = "9";
			Answers[3][2] = "25";
			Answers[3][3] = "11";

			Answers[4][0] = "Sudbury";
			Answers[4][1] = "Montreal";
			Answers[4][2] = "Ottawa";
			Answers[4][3] = "Toronto";

			Answers[5][0] = "1848";
			Answers[5][1] = "1840";
			Answers[5][2] = "1855";
			Answers[5][3] = "1856";

			Answers[6][0] = "";
			Answers[6][1] = "";
			Answers[6][2] = "";
			Answers[6][3] = "";

			Answers[7][0] = "4";
			Answers[7][1] = "6";
			Answers[7][2] = "7";
			Answers[7][3] = "8";

			Answers[8][0] = "Franc";
			Answers[8][1] = "Euro";
			Answers[8][2] = "Krone";
			Answers[8][3] = "Peso";

			Answers[9][0] = "Portuguese";
			Answers[9][1] = "Spanish";
			Answers[9][2] = "English";
			Answers[9][3] = "French";

			Answers[10][0] = "1121";
			Answers[10][1] = "2347";
			Answers[10][2] = "912";
			Answers[10][3] = "623";

			Answers[11][0] = "Eurasian and South American";
			Answers[11][1] = "Eurasian and Antarctic";
			Answers[11][2] = "Eurasian and North American";
			Answers[11][3] = "Eurasian and Indo-Australian";

			Answers[12][0] = "Baikal, Russia";
			Answers[12][1] = "Vostok, Antarctica";
			Answers[12][2] = "Issyk-Kul, Kyrgyzstan";
			Answers[12][3] = "Tanganyika, Africa";

			Answers[13][0] = "Seine River";
			Answers[13][1] = "Garonne";
			Answers[13][2] = "Loire";
			Answers[13][3] = "Rhin";

			Answers[14][0] = "Indian";
			Answers[14][1] = "Atlantic";
			Answers[14][2] = "Arctic";
			Answers[14][3] = "Pacific";

			Answers[15][0] = "Russia";
			Answers[15][1] = "China";
			Answers[15][2] = "India";
			Answers[15][3] = "France";

			Answers[16][0] = "City of Naples";
			Answers[16][1] = "Rome";
			Answers[16][2] = "Milan";
			Answers[16][3] = "Turin";

			Answers[17][0] = "Birmingham";
			Answers[17][1] = "Manchester";
			Answers[17][2] = "London";
			Answers[17][3] = "Hull";

			Answers[18][0] = "Drachma";
			Answers[18][1] = "Shillings";
			Answers[18][2] = "Rupees";
			Answers[18][3] = "Pound";

			Answers[19][0] = "Vietnamese";
			Answers[19][1] = "China";
			Answers[19][2] = "France";
			Answers[19][3] = "Iraq";

			/*Init Geography Correct Answers*/
			CorrectAnswer[0] = Answers[0][0];
			CorrectAnswer[1] = Answers[1][0];
			CorrectAnswer[2] = Answers[2][0];
			CorrectAnswer[3] = Answers[3][0];
			CorrectAnswer[4] = Answers[4][0];
			CorrectAnswer[5] = Answers[5][0];
			CorrectAnswer[6] = Answers[6][0];
			CorrectAnswer[7] = Answers[7][0];
			CorrectAnswer[8] = Answers[8][0];
			CorrectAnswer[9] = Answers[9][0];
			CorrectAnswer[10] = Answers[10][0];
			CorrectAnswer[11] = Answers[11][0];
			CorrectAnswer[12] = Answers[12][0];
			CorrectAnswer[13] = Answers[13][0];
			CorrectAnswer[14] = Answers[14][0];
			CorrectAnswer[15] = Answers[15][0];
			CorrectAnswer[16] = Answers[16][0];
			CorrectAnswer[17] = Answers[17][0];
			CorrectAnswer[18] = Answers[18][0];
			CorrectAnswer[19] = Answers[19][0];
		}
	}

	if (subject == Subjects[7])
	{
		if (set == 0)
		{
			/*Init General Knowledge Questions*/
			Questions[0] = "What is the capital of Poland?";
			Questions[1] = "Which musician�s real name is Reginald Kenneth Dwight?";
			Questions[2] = "What was the most downloaded app of 2020?";
			Questions[3] = "Europe is separated from Africa by which sea?";
			Questions[4] = "What is the collective name for a group of crows?";
			Questions[5] = "Which Coronation Street character has been married six times?";
			Questions[6] = "What is Japanese sake made from?";
			Questions[7] = "Who is the only person in the UK who is allowed to drive without a licence?";
			Questions[8] = "How many countries still have the shilling as currency?";
			Questions[9] = "Who has been forced out of Dancing On Ice by a positive coronavirus test?";
			Questions[10] = "Which rock band has topped the UK album chart for a fifth time with Medicine At Midnight?";
			Questions[11] = "Which actress will not return to Disney Plus series The Mandalorian following �abhorrent and unacceptable� social media posts, according to Lucasfilm?";
			Questions[12] = "many Olympic gold medals has Mo Farah won?";
			Questions[13] = "What is the Chinese New Year animal for 2021?";
			Questions[14] = "Which Beatle crossed Abbey Road first?";
			Questions[15] = "Enchiladas originated in which country?";
			Questions[16] = "What colour is a giraffe's tongue?";
			Questions[17] = "Which country was the first to give women the right to vote, in 1893?";
			Questions[18] = "How many teeth does the average adult human have?";
			Questions[19] = "Brothers Richard and Maurice founded which company in 1940?";

			/*Init General Knowledge Answers*/
			Answers[0][0] = "Warsaw";
			Answers[0][1] = "Krakow";
			Answers[0][2] = "Wroclaw";
			Answers[0][3] = "Lodz";

			Answers[1][0] = "Elton John";
			Answers[1][1] = "David Jones";
			Answers[1][2] = "Farrokh Bulsara";
			Answers[1][3] = "Robbie William";

			Answers[2][0] = "Tik Tok";
			Answers[2][1] = "WhatsApp";
			Answers[2][2] = "Facebook";
			Answers[2][3] = "Instagram";

			Answers[3][0] = "Mediterranean Sea";
			Answers[3][1] = "Red sea";
			Answers[3][2] = "Nile";
			Answers[3][3] = "Yangtze";

			Answers[4][0] = "Murder";
			Answers[4][1] = "Crowers";
			Answers[4][2] = "Cromers";
			Answers[4][3] = "Flock";

			Answers[5][0] = "Steve McDonald";
			Answers[5][1] = "Gail Mclntyre";
			Answers[5][2] = "Ken Barlow";
			Answers[5][3] = "Simon Barlow";

			Answers[6][0] = "Fermented rice";
			Answers[6][1] = "Jasmine flower";
			Answers[6][2] = "Tree leaves";
			Answers[6][3] = "Seaweed";

			Answers[7][0] = "The Queen";
			Answers[7][1] = "The prime minister";
			Answers[7][2] = "Prince Philip";
			Answers[7][3] = "Prince Charles";

			Answers[8][0] = "5";
			Answers[8][1] = "4";
			Answers[8][2] = "3";
			Answers[8][3] = "6";

			Answers[9][0] = "Joe Warren";
			Answers[9][1] = "Libby Clegg";
			Answers[9][2] = "Jose Swash";
			Answers[9][3] = "Maura Higgins";

			Answers[10][0] = "Foo Fighters";
			Answers[10][1] = "Beatles";
			Answers[10][2] = "Metal Singles";
			Answers[10][3] = "Arctic Monkeys";

			Answers[11][0] = "Gina Carano ";
			Answers[11][1] = "Katee Sackhoff";
			Answers[11][2] = "Ming-Na Wen";
			Answers[11][3] = "Baby Yoda";

			Answers[12][0] = "3";
			Answers[12][1] = "4";
			Answers[12][2] = "5";
			Answers[12][3] = "7";

			Answers[13][0] = "Ox";
			Answers[13][1] = "Rat";
			Answers[13][2] = "Rabbit";
			Answers[13][3] = "Snake";

			Answers[14][0] = "John Lennon";
			Answers[14][1] = "Paul McCartney";
			Answers[14][2] = "Ringo Starr";
			Answers[14][3] = "George Harrison";

			Answers[15][0] = "Mexico";
			Answers[15][1] = "Spain";
			Answers[15][2] = "Italy";
			Answers[15][3] = "Ireland";

			Answers[16][0] = "Black";
			Answers[16][1] = "Blue";
			Answers[16][2] = "Brown";
			Answers[16][3] = "Yellow";

			Answers[17][0] = "New Zealand";
			Answers[17][1] = "England";
			Answers[17][2] = "Itay";
			Answers[17][3] = "Greenland";

			Answers[18][0] = "30";
			Answers[18][1] = "32";
			Answers[18][2] = "26";
			Answers[18][3] = "92";

			Answers[19][0] = "McDonald�s";
			Answers[19][1] = "KFC";
			Answers[19][2] = "Burger King";
			Answers[19][3] = "Nandos";

			/*Init General Knowledge Correct Answers*/
			CorrectAnswer[0] = Answers[0][0];
			CorrectAnswer[1] = Answers[1][0];
			CorrectAnswer[2] = Answers[2][0];
			CorrectAnswer[3] = Answers[3][0];
			CorrectAnswer[4] = Answers[4][0];
			CorrectAnswer[5] = Answers[5][0];
			CorrectAnswer[6] = Answers[6][0];
			CorrectAnswer[7] = Answers[7][0];
			CorrectAnswer[8] = Answers[8][0];
			CorrectAnswer[9] = Answers[9][0];
			CorrectAnswer[10] = Answers[10][0];
			CorrectAnswer[11] = Answers[11][0];
			CorrectAnswer[12] = Answers[12][0];
			CorrectAnswer[13] = Answers[13][0];
			CorrectAnswer[14] = Answers[14][0];
			CorrectAnswer[15] = Answers[15][0];
			CorrectAnswer[16] = Answers[16][0];
			CorrectAnswer[17] = Answers[17][0];
			CorrectAnswer[18] = Answers[18][0];
			CorrectAnswer[19] = Answers[19][0];
		}

		if (set == 1)
		{
			/*Init General Knowledge Questions*/
			Questions[0] = "What year was the first series of X Factor?";
			Questions[1] = "What is the longest running soap opera in the UK?";
			Questions[2] = "The Troubles in Northern Ireland were brought to an end in 1998 with the signing of what document?";
			Questions[3] = "What is the name of the headteacher in Roald Dahl's Matilda?";
			Questions[4] = "Who is the vice president of the US?";
			Questions[5] = "In Greek mythology, Perseus slays which monster with snakes for hair?";
			Questions[6] = "Vanilla comes from what flowers?";
			Questions[7] = "What is the real name of The Vixen on ITV's The Chase?";
			Questions[8] = "What is the painting �La Gioconda� more usually known as?";
			Questions[9] = "Z and which other letter are worth the most in Scrabble?";
			Questions[10] = "What was the name of Woolworths had its own children�s clothing brand?";
			Questions[11] = "Who did Queen Elizabeth II surpass as Britain's longest serving monarch in September 2015?";
			Questions[12] = "When did the Cold War end?";
			Questions[13] = "How many sitting US presidents have been assassinated?";
			Questions[14] = "How many pounds are in a stone?";
			Questions[15] = "The Chronicles of Narnia' is a children's book series written by which author?";
			Questions[16] = "What pop singer�s real name is Katy Hudson?";
			Questions[17] = "Which art movement is Salvador Dali associated with?";
			Questions[18] = "Which is the eighth and farthest-known planet from the sun in the solar system?";
			Questions[19] = "'S. O. S.' is a common example of which electrical telegraph communication system?";

			/*Init General Knowledge Answers*/
			Answers[0][0] = "2004";
			Answers[0][1] = "2005";
			Answers[0][2] = "2003";
			Answers[0][3] = "2002";

			Answers[1][0] = "Coronation Street";
			Answers[1][1] = "EastEnders";
			Answers[1][2] = "Emmerdale";
			Answers[1][3] = "Frasier";

			Answers[2][0] = "The Good Friday Agreement";
			Answers[2][1] = "Peace Agreement";
			Answers[2][2] = "Rights Act";
			Answers[2][3] = "Equality Act";

			Answers[3][0] = "Miss Trunchbull";
			Answers[3][1] = "Miss Hunrgyson";
			Answers[3][2] = "Miss Lozepz";
			Answers[3][3] = "Mrs Hardly";

			Answers[4][0] = "Kamala Harris";
			Answers[4][1] = "Bill Clinton";
			Answers[4][2] = "Barack Obama";
			Answers[4][3] = "Hon Priti Patel";

			Answers[5][0] = "Medusa";
			Answers[5][1] = "Sickama";
			Answers[5][2] = "Strike eyes";
			Answers[5][3] = "Brown";

			Answers[6][0] = "Orchids";
			Answers[6][1] = "Lilly";
			Answers[6][2] = "Lotus";
			Answers[6][3] = "Yasmin";

			Answers[7][0] = "Jenny Rayan";
			Answers[7][1] = "Anne Hegerty";
			Answers[7][2] = "Shaun Wallace";
			Answers[7][3] = "Ann Leigh";

			Answers[8][0] = "The Mona Lisa";
			Answers[8][1] = "Girl with pearl earrings";
			Answers[8][2] = "Guernica";
			Answers[8][3] = "Box painting";

			Answers[9][0] = "Q";
			Answers[9][1] = "L";
			Answers[9][2] = "K";
			Answers[9][3] = "S";

			Answers[10][0] = "LadyBird";
			Answers[10][1] = "Plantitude";
			Answers[10][2] = "FF";
			Answers[10][3] = "GoodClothing";

			Answers[11][0] = "Queen Victoria";
			Answers[11][1] = "Queen Elizbeth 1";
			Answers[11][2] = "Anne";
			Answers[11][3] = "Henry V";

			Answers[12][0] = "1989";
			Answers[12][1] = "1980";
			Answers[12][2] = "1988";
			Answers[12][3] = "1982";

			Answers[13][0] = "3";
			Answers[13][1] = "4";
			Answers[13][2] = "5";
			Answers[13][3] = "6";

			Answers[14][0] = "14";
			Answers[14][1] = "15";
			Answers[14][2] = "13";
			Answers[14][3] = "12";

			Answers[15][0] = "CS Lewis";
			Answers[15][1] = "Roald Dahl";
			Answers[15][2] = "J.k. Rowling";
			Answers[15][3] = "Jade Huges";

			Answers[16][0] = "Katheryn Elizabeth Hudson";
			Answers[16][1] = "Katy Perry";
			Answers[16][2] = "Katheryn Highland";
			Answers[16][3] = "Kathy Highmend";

			Answers[17][0] = "Surrealism";
			Answers[17][1] = "Modern art";
			Answers[17][2] = "Cubism";
			Answers[17][3] = "Futurism";

			Answers[18][0] = "Neptune";
			Answers[18][1] = "Mars";
			Answers[18][2] = "Earth";
			Answers[18][3] = "Pluto";

			Answers[19][0] = "Morse code";
			Answers[19][1] = "Tap Code";
			Answers[19][2] = "Pigpen Cipher";
			Answers[19][3] = "Scytale";

			/*Init General Knowledge Correct Answers*/
			CorrectAnswer[0] = Answers[0][0];
			CorrectAnswer[1] = Answers[1][0];
			CorrectAnswer[2] = Answers[2][0];
			CorrectAnswer[3] = Answers[3][0];
			CorrectAnswer[4] = Answers[4][0];
			CorrectAnswer[5] = Answers[5][0];
			CorrectAnswer[6] = Answers[6][0];
			CorrectAnswer[7] = Answers[7][0];
			CorrectAnswer[8] = Answers[8][0];
			CorrectAnswer[9] = Answers[9][0];
			CorrectAnswer[10] = Answers[10][0];
			CorrectAnswer[11] = Answers[11][0];
			CorrectAnswer[12] = Answers[12][0];
			CorrectAnswer[13] = Answers[13][0];
			CorrectAnswer[14] = Answers[14][0];
			CorrectAnswer[15] = Answers[15][0];
			CorrectAnswer[16] = Answers[16][0];
			CorrectAnswer[17] = Answers[17][0];
			CorrectAnswer[18] = Answers[18][0];
			CorrectAnswer[19] = Answers[19][0];
		}

		if (set == 2)
		{
			/*Init General Knowledge Questions*/
			Questions[0] = "How many Amendments does the US Constitution have?";
			Questions[1] = "What type of rock is marble?";
			Questions[2] = "Which British political figure became Baroness Kesteven?";
			Questions[3] = "Who took the assumed name Sebastian Melmoth when living in Paris?";
			Questions[4] = "What is a clerihew?";
			Questions[5] = "What musical term indicates a chord where the notes are played one after another rather than all together?";
			Questions[6] = "According to the famous nursery rhyme, who found Lucy Lockets Pocket?";
			Questions[7] = "What fruit is used to make Slivovitz?";
			Questions[8] = "What is the collective noun for a group of giraffes?";
			Questions[9] = "What year did Vincent Van Gogh die?";
			Questions[10] = "When was the first computer invented?";
			Questions[11] = "What type of clothing is a Glengarry?";
			Questions[12] = "What is the largest moon of Saturn called?";
			Questions[13] = "What is the Olympic sport in which athletes cross the finish line backwards?";
			Questions[14] = "How many black and white squares are there on a chess board in total?";
			Questions[15] = "The wood of a cricket bat is traditionally made from which type of tree?";
			Questions[16] = "How many elements are there in the periodic table?";
			Questions[17] = "What month was Prince George born?";
			Questions[18] = "What is the Papaver Rhoeas flower better known as?";
			Questions[19] = "What year was Google images invented, and for a bonus point, what prompted its creation?";

			/*Init General Knowledge Answers*/
			Answers[0][0] = "27";
			Answers[0][1] = "21";
			Answers[0][2] = "22";
			Answers[0][3] = "52";

			Answers[1][0] = "Metamorphic";
			Answers[1][1] = "Igneous";
			Answers[1][2] = "Sedimentary";
			Answers[1][3] = "Magama";

			Answers[2][0] = "Margaret Thatcher";
			Answers[2][1] = "Geoffrey Howe";
			Answers[2][2] = "Ian Davey";
			Answers[2][3] = "Keir Starmer";

			Answers[3][0] = "Oscar Wilde";
			Answers[3][1] = "Ivy Bannister";
			Answers[3][2] = "James Joyce";
			Answers[3][3] = "Eavan Boland";

			Answers[4][0] = "Poem";
			Answers[4][1] = "Song";
			Answers[4][2] = "Rap";
			Answers[4][3] = "Saying";

			Answers[5][0] = "Arpeggio";
			Answers[5][1] = "Adagio";
			Answers[5][2] = "Cadence";
			Answers[5][3] = "Accent";

			Answers[6][0] = "Kitty Fisher";
			Answers[6][1] = "Matt  Matthew";
			Answers[6][2] = "Kevin Ledge";
			Answers[6][3] = "Lea Thrope";

			Answers[7][0] = "Damson plums";
			Answers[7][1] = "Apples";
			Answers[7][2] = "Cherry";
			Answers[7][3] = "Green grapes";

			Answers[8][0] = "Tower";
			Answers[8][1] = "Giraffy";
			Answers[8][2] = "Giraff";
			Answers[8][3] = "Long heads";

			Answers[9][0] = "1890";
			Answers[9][1] = "1990";
			Answers[9][2] = "1920";
			Answers[9][3] = "1860";

			Answers[10][0] = "1946";
			Answers[10][1] = "1980";
			Answers[10][2] = "1955";
			Answers[10][3] = "1947";

			Answers[11][0] = "Hat";
			Answers[11][1] = "Socks";
			Answers[11][2] = "Shirt";
			Answers[11][3] = "Headband";

			Answers[12][0] = "Titan";
			Answers[12][1] = "Enceladus";
			Answers[12][2] = "Mimas";
			Answers[12][3] = "Dione";

			Answers[13][0] = "Rowing";
			Answers[13][1] = "High Jump";
			Answers[13][2] = "Long Jump";
			Answers[13][3] = "Backstroke";

			Answers[14][0] = "64";
			Answers[14][1] = "65";
			Answers[14][2] = "67";
			Answers[14][3] = "68";

			Answers[15][0] = "Willow";
			Answers[15][1] = "Oil";
			Answers[15][2] = "Oak";
			Answers[15][3] = "Maple";

			Answers[16][0] = "118";
			Answers[16][1] = "110";
			Answers[16][2] = "121";
			Answers[16][3] = "117";

			Answers[17][0] = "July";
			Answers[17][1] = "January";
			Answers[17][2] = "February";
			Answers[17][3] = "December";

			Answers[18][0] = "Poppy";
			Answers[18][1] = "Phlox";
			Answers[18][2] = "Petunia";
			Answers[18][3] = "Peruvian Lily";

			Answers[19][0] = "2001";
			Answers[19][1] = "2002";
			Answers[19][2] = "1999";
			Answers[19][3] = "2000";

			/*Init General Knowledge Correct Answers*/
			CorrectAnswer[0] = Answers[0][0];
			CorrectAnswer[1] = Answers[1][0];
			CorrectAnswer[2] = Answers[2][0];
			CorrectAnswer[3] = Answers[3][0];
			CorrectAnswer[4] = Answers[4][0];
			CorrectAnswer[5] = Answers[5][0];
			CorrectAnswer[6] = Answers[6][0];
			CorrectAnswer[7] = Answers[7][0];
			CorrectAnswer[8] = Answers[8][0];
			CorrectAnswer[9] = Answers[9][0];
			CorrectAnswer[10] = Answers[10][0];
			CorrectAnswer[11] = Answers[11][0];
			CorrectAnswer[12] = Answers[12][0];
			CorrectAnswer[13] = Answers[13][0];
			CorrectAnswer[14] = Answers[14][0];
			CorrectAnswer[15] = Answers[15][0];
			CorrectAnswer[16] = Answers[16][0];
			CorrectAnswer[17] = Answers[17][0];
			CorrectAnswer[18] = Answers[18][0];
			CorrectAnswer[19] = Answers[19][0];
		}
	}

	if (subject == Subjects[8])
	{
		if (set == 0)
		{
			/*Init Literature Questions*/
			Questions[0] = "Tana French�s mystery series that starts with In the Woods is based in which city?";
			Questions[1] = "Who is the heroine of Janet Evanovich�s One for the Money and its sequels?";
			Questions[2] = "Name That Author: Who wrote The Girl on the Train?";
			Questions[3] = "Which is NOT a poem by Emily Dickinson?";
			Questions[4] = "Name That Poet: Who wrote the Beat classic �Howl�?";
			Questions[5] = "Which two poets co-wrote the Lyrical Ballads?";
			Questions[6] = "When was Edgar Allan Poe�s �The Raven� first published?";
			Questions[7] = "Name That Poet: Which poet associated with Romanticism wrote Songs of Innocence and Experience?";
			Questions[8] = "Who wrote I Know Why the Caged Bird Sings?";
			Questions[9] = "How many acts are in Romeo and Juliet?";
			Questions[10] = "The character of Hamlet was the ruler of which country?";
			Questions[11] = "Who is the youngest daughter of King Lear?";
			Questions[12] = "Which of these ingredients does NOT appear in the Witches� �Double, double toil and trouble� speech from Macbeth?";
			Questions[13] = "William Faulkner�s The Sound and the Fury takes its title from which Shakespeare plays?";
			Questions[14] = "Which of these plays is NOT considered to be one of Shakespeare�s comedies?";
			Questions[15] = "Who wrote the 1936 novel �Gone with the Wind�, on which the 1939 film of the same name was based?";
			Questions[16] = "Which Shakespearean play features the characters of Goneril, Regan and Cordelia?";
			Questions[17] = "What is the first name of Professor McGonagall in the Harry Potter books?";
			Questions[18] = "Which book of the New Testament comes after the four gospels of Matthew, Mark, Luke and John?";
			Questions[19] = "Who wrote Invisible Man in 1952?";

			/*Init Literature Answers*/
			Answers[0][0] = "Dublin";
			Answers[0][1] = "London";
			Answers[0][2] = "Edinburgh";
			Answers[0][3] = "Belfast";

			Answers[1][0] = "Stephanie Plum";
			Answers[1][1] = "Sadie Pear";
			Answers[1][2] = "Stacy Partridge";
			Answers[1][3] = "Sarah Peach";

			Answers[2][0] = "Paula Hawkins";
			Answers[2][1] = "A.J. Finn";
			Answers[2][2] = "Sophie Hannah";
			Answers[2][3] = "Ruth Ware";

			Answers[3][0] = "Acquainted with the night";
			Answers[3][1] = "Hope� is the thing with feathers";
			Answers[3][2] = "Because I could not stop for Death";
			Answers[3][3] = "I felt a Funeral, in my Brain";

			Answers[4][0] = "Allen Ginsberg";
			Answers[4][1] = "Jack Kerouac";
			Answers[4][2] = "Lawrence Ferlinghetti";
			Answers[4][3] = "William Carlos Williams";

			Answers[5][0] = "William Wordsworth and Samuel Taylor Coleridge";
			Answers[5][1] = "T.S. Eliot and Ezra Pound";
			Answers[5][2] = "Sylvia Plath and Anne Sexton";
			Answers[5][3] = "Robert Lowell and Elizabeth Bishop";

			Answers[6][0] = "1845";
			Answers[6][1] = "1838";
			Answers[6][2] = "1840";
			Answers[6][3] = "1850";

			Answers[7][0] = "William Blake";
			Answers[7][1] = "John Keats";
			Answers[7][2] = "William Wordsworth";
			Answers[7][3] = "Percy Bysshe Shelley";

			Answers[8][0] = "Maya Angelou";
			Answers[8][1] = "Audre Lorde";
			Answers[8][2] = "Adrienne Rich";
			Answers[8][3] = "Gwendolyn Brooks";

			Answers[9][0] = "5";
			Answers[9][1] = "4";
			Answers[9][2] = "3";
			Answers[9][3] = "7";

			Answers[10][0] = "Denmark";
			Answers[10][1] = "Scotland";
			Answers[10][2] = "Sweden";
			Answers[10][3] = "England";

			Answers[11][0] = "Cordelia";
			Answers[11][1] = "Ophelia";
			Answers[11][2] = "Regan";
			Answers[11][3] = "Goneril";

			Answers[12][0] = "Wing of bat";
			Answers[12][1] = "Eye of newt";
			Answers[12][2] = "Lizard�s leg";
			Answers[12][3] = "Tongue of dog";

			Answers[13][0] = "Hamlet";
			Answers[13][1] = "King Lear";
			Answers[13][2] = "Romeo and Juliet";
			Answers[13][3] = "Much Ado About Nothing";

			Answers[14][0] = "Cymbeline";
			Answers[14][1] = "Two Gentlemen of Verona";
			Answers[14][2] = "Merchant of Venice";
			Answers[14][3] = "Winter�s Tale";

			Answers[15][0] = "Margaret Mitchell";
			Answers[15][1] = "Jane Johnson";
			Answers[15][2] = "Merthyr Mathew";
			Answers[15][3] = "Bethany Harrison";

			Answers[16][0] = "King Lear";
			Answers[16][1] = "Macbeth";
			Answers[16][2] = "Julius Caesar";
			Answers[16][3] = "The Tempest";

			Answers[17][0] = "Minerva";
			Answers[17][1] = "Martian";
			Answers[17][2] = "Matthias";
			Answers[17][3] = "Marcel";

			Answers[18][0] = "Acts";
			Answers[18][1] = "Romans";
			Answers[18][2] = "Philippians";
			Answers[18][3] = "Job";

			Answers[19][0] = "Ralph Ellison";
			Answers[19][1] = "Ray Bradbury";
			Answers[19][2] = "Allen Ginsberg";
			Answers[19][3] = "Ayn Rand";

			/*Init Literature Correct Answers*/
			CorrectAnswer[0] = Answers[0][0];
			CorrectAnswer[1] = Answers[1][0];
			CorrectAnswer[2] = Answers[2][0];
			CorrectAnswer[3] = Answers[3][0];
			CorrectAnswer[4] = Answers[4][0];
			CorrectAnswer[5] = Answers[5][0];
			CorrectAnswer[6] = Answers[6][0];
			CorrectAnswer[7] = Answers[7][0];
			CorrectAnswer[8] = Answers[8][0];
			CorrectAnswer[9] = Answers[9][0];
			CorrectAnswer[10] = Answers[10][0];
			CorrectAnswer[11] = Answers[11][0];
			CorrectAnswer[12] = Answers[12][0];
			CorrectAnswer[13] = Answers[13][0];
			CorrectAnswer[14] = Answers[14][0];
			CorrectAnswer[15] = Answers[15][0];
			CorrectAnswer[16] = Answers[16][0];
			CorrectAnswer[17] = Answers[17][0];
			CorrectAnswer[18] = Answers[18][0];
			CorrectAnswer[19] = Answers[19][0];
		}

		if (set == 1)
		{
			/*Init Literature Questions*/
			Questions[0] = "In Charles Dickens�s Oliver Twist, what food does Oliver famously ask for more of when he says: �Please, Sir, I want some more�?";
			Questions[1] = "Emily Bront�s Wuthering Heights recounts the tragic romance between which two lovers?";
			Questions[2] = "Who is the heroine of Nathaniel Hawthorne�s The Scarlet Letter?";
			Questions[3] = "Author Zora Neale Hurston was part of which literary movement?";
			Questions[4] = "Truman Capote�s In Cold Blood takes its name from which Shakespeare play?";
			Questions[5] = "Who is the protagonist of James Joyce�s Ulysses?";
			Questions[6] = "Who wrote The Electric Kool-Aid Acid Test?";
			Questions[7] = "n J.D. Salinger�s The Catcher in the Rye, protagonist Holden Caulfield worries about how which animal survives the winter in New York?";
			Questions[8] = "How many volumes are in Marcel Proust�s novel � La Recherche du Temps Perdu (�In Search of Lost Time�)?";
			Questions[9] = "The Girl with the Dragoon Tattoo was originally published in which language?";
			Questions[10] = "In what year was Gillian Flynn�s domestic thriller Gone Girl published?";
			Questions[11] = "E.L. James�s Fifty Shades of Grey was originally fan fiction for which book series?";
			Questions[12] = "Name That Book: What is the seventh and final instalment in J. K. Rowling�s Harry Potter series?";
			Questions[13] = "In Erin Morgenstern�s The Night Circus, two star-crossed lovers are rivals. What profession do they share?";
			Questions[14] = "Who was the first Chinese writer to win the Nobel Prize for Literature?";
			Questions[15] = "What was the working title of Margaret Mitchell's 1936 novel Gone With the Wind?";
			Questions[16] = "What famous literary character offers this sage advice: 'Neither a borrower nor a lender be'?";
			Questions[17] = "What novel is set on a desert planet inhabited by giant sandworms?";
			Questions[18] = "Which Stephen King novel takes place mostly in the fictional Overlook Hotel?";
			Questions[19] = "What is the best selling novel of all time?";

			/*Init Literature Answers*/
			Answers[0][0] = "Gruel";
			Answers[0][1] = "Bread";
			Answers[0][2] = "Soup";
			Answers[0][3] = "Cheese";

			Answers[1][0] = "Catherine and Heathcliff";
			Answers[1][1] = "Eleanor and Christian";
			Answers[1][2] = "Isabella and Hindley";
			Answers[1][3] = "Anne and Gordon";

			Answers[2][0] = "Hester Prynne";
			Answers[2][1] = "Anne Hutchinson";
			Answers[2][2] = "Emily Shelby";
			Answers[2][3] = "Agnes Grey";

			Answers[3][0] = "Harlem Renaissance";
			Answers[3][1] = "Romanticism";
			Answers[3][2] = "Transcendentalism";
			Answers[3][3] = "Beat";

			Answers[4][0] = "Timon of Athens";
			Answers[4][1] = "Macbeth";
			Answers[4][2] = "Othello";
			Answers[4][3] = "Hamlet";

			Answers[5][0] = "Leopold Bloom";
			Answers[5][1] = "Leopold Ulysses";
			Answers[5][2] = "Ulysses Bloom";
			Answers[5][3] = "Homer Bloom";

			Answers[6][0] = "Tom Wolfe";
			Answers[6][1] = "William S. Burroughs";
			Answers[6][2] = "Hunter S. Thompson";
			Answers[6][3] = "Ken Kesey";

			Answers[7][0] = "Ducks";
			Answers[7][1] = "Pigeons";
			Answers[7][2] = "Cats";
			Answers[7][3] = "Squirrels";

			Answers[8][0] = "7";
			Answers[8][1] = "3";
			Answers[8][2] = "5";
			Answers[8][3] = "6";

			Answers[9][0] = "Swedish";
			Answers[9][1] = "Norwegian";
			Answers[9][2] = "Danish";
			Answers[9][3] = "English";

			Answers[10][0] = "2013";
			Answers[10][1] = "2012";
			Answers[10][2] = "2014";
			Answers[10][3] = "2016";

			Answers[11][0] = "The Harry Potter Series";
			Answers[11][1] = "The Twilight Series";
			Answers[11][2] = "The Outlander Series";
			Answers[11][3] = "The Left Behind Series";

			Answers[12][0] = "Harry Potter and the Deathly Hallows";
			Answers[12][1] = "Harry Potter and the Order of the Phoenix";
			Answers[12][2] = "Harry Potter and the Half-Blood Prince";
			Answers[12][3] = "Harry Potter and the Cursed Child";

			Answers[13][0] = "Magicians";
			Answers[13][1] = "Acrobats";
			Answers[13][2] = "Tightrope Walkers";
			Answers[13][3] = "Trapeze Artists";

			Answers[14][0] = "Gao Xingjian";
			Answers[14][1] = "BA Jin";
			Answers[14][2] = "Zhang Aling";
			Answers[14][3] = "Cao Yu";

			Answers[15][0] = "Baa Baa Black Sheep";
			Answers[15][1] = "The Rhett Butler Affair";
			Answers[15][2] = "Plantation";
			Answers[15][3] = "Tara";

			Answers[16][0] = "Polonius";
			Answers[16][1] = "Tom Sawyer";
			Answers[16][2] = "Albus Dumbledore";
			Answers[16][3] = "Ebenezer Scrooge";

			Answers[17][0] = "Dune";
			Answers[17][1] = "Do androids dream of electric sheep";
			Answers[17][2] = "Stranger in strange land";
			Answers[17][3] = "Foundation";

			Answers[18][0] = "The Shining";
			Answers[18][1] = "Doctor Sleep";
			Answers[18][2] = "Salem�s Lot";
			Answers[18][3] = "It";

			Answers[19][0] = "Don Quixote";
			Answers[19][1] = "The Hobbit";
			Answers[19][2] = "Oby Dickson";
			Answers[19][3] = "Harry potter and the philosopher's stone";

			/*Init Literature Correct Answers*/
			CorrectAnswer[0] = Answers[0][0];
			CorrectAnswer[1] = Answers[1][0];
			CorrectAnswer[2] = Answers[2][0];
			CorrectAnswer[3] = Answers[3][0];
			CorrectAnswer[4] = Answers[4][0];
			CorrectAnswer[5] = Answers[5][0];
			CorrectAnswer[6] = Answers[6][0];
			CorrectAnswer[7] = Answers[7][0];
			CorrectAnswer[8] = Answers[8][0];
			CorrectAnswer[9] = Answers[9][0];
			CorrectAnswer[10] = Answers[10][0];
			CorrectAnswer[11] = Answers[11][0];
			CorrectAnswer[12] = Answers[12][0];
			CorrectAnswer[13] = Answers[13][0];
			CorrectAnswer[14] = Answers[14][0];
			CorrectAnswer[15] = Answers[15][0];
			CorrectAnswer[16] = Answers[16][0];
			CorrectAnswer[17] = Answers[17][0];
			CorrectAnswer[18] = Answers[18][0];
			CorrectAnswer[19] = Answers[19][0];
		}

		if (set == 2)
		{
			/*Init Literature Questions*/
			Questions[0] = "Name That Author: Who wrote The Iliad?";
			Questions[1] = "Which of these are the three cantos of The Divine Comedy in the correct order?";
			Questions[2] = "Don Quixote was written in which language?";
			Questions[3] = "The hero Beowulf faces a monster known by which name?";
			Questions[4] = "John Donne is known as a member of which school of poetry?";
			Questions[5] = "John Milton�s Paradise Lost was written during which century?";
			Questions[6] = "Mark Twain�s beloved characters Tom Sawyer and Huckleberry Finn reside in which state?";
			Questions[7] = "This genre of fiction, in which novels like Dracula are told in letters, diary entries, newspaper clippings, and more mixed formats, came to prominence in the late 18th century?";
			Questions[8] = "Sir Walter Scott is known for his historical novels about which country?";
			Questions[9] = "In Stephen King�s 11/22/63, a high school English teacher hurtles back in time to 1963 and attempts to stop what monumental event?";
			Questions[10] = "Which author wrote the Pulitzer Prize winning novel The Goldfinch?";
			Questions[11] = "Who is the hero of Douglas Adams�s Hitchhiker�s Guide to the Galaxy series?";
			Questions[12] = "Stanley Kubrick�s 1968 epic sci-fi film 2001: A Space Odyssey was inspired by a short story by which writer, who also co-wrote the screenplay with Kubrick?";
			Questions[13] = "Neil Gaiman�s Neverwhere is set in an underground world of which city?";
			Questions[14] = "Who is the central hero of Patrick Rothfuss�s The Name of the Wind?";
			Questions[15] = "Which book chronologically, comes first in C. S. Lewis�s Chronicles of Narnia series?";
			Questions[16] = "What�s the name of the college of magic that Quentin Coldwater attends in Lev Grossman�s Magicians trilogy?";
			Questions[17] = "Sir Arthur Conan Doyle�s Sherlock Holmes made his first appearance in print with which novel?";
			Questions[18] = "Which author  wrote the noir classic The Maltese Falcon?";
			Questions[19] = "In which century was the Florentine painter known as Sandro Botticelli born?";

			/*Init Literature Answers*/
			Answers[0][0] = "Homer";
			Answers[0][1] = "Euripides";
			Answers[0][2] = "Beowulf";
			Answers[0][3] = "Virgil";

			Answers[1][0] = "Inferno, Purgatorio, Paradiso";
			Answers[1][1] = "Paradiso, Purgatorio, Inferno";
			Answers[1][2] = "Inferno, Limbo, Paradiso";
			Answers[1][3] = "Purgatorio, Inferno, Limbo";

			Answers[2][0] = "Spanish";
			Answers[2][1] = "Old English";
			Answers[2][2] = "Italian";
			Answers[2][3] = "Greek";

			Answers[3][0] = "Minotaur";
			Answers[3][1] = "Hrothgar";
			Answers[3][2] = "Grendel";
			Answers[3][3] = "Heorot";

			Answers[4][0] = "Georgian";
			Answers[4][1] = "Romanticism";
			Answers[4][2] = "Jacobean";
			Answers[4][3] = "Metaphysical";

			Answers[5][0] = "1600�s";
			Answers[5][1] = "1400�s";
			Answers[5][2] = "1500�s";
			Answers[5][3] = "1700�s";

			Answers[6][0] = "Mississippi";
			Answers[6][1] = "Alabama";
			Answers[6][2] = "Missouri";
			Answers[6][3] = "Tennessee";

			Answers[7][0] = "Epistolary";
			Answers[7][1] = "Belles-lettres";
			Answers[7][2] = "Pastoral";
			Answers[7][3] = "Bildungsroman";

			Answers[8][0] = "Scotland";
			Answers[8][1] = "Ireland";
			Answers[8][2] = "Wales";
			Answers[8][3] = "England";

			Answers[9][0] = "The assassination of President John F. Kennedy";
			Answers[9][1] = "The moon landing of Apollo 11";
			Answers[9][2] = "The Cuban Missile Crisis";
			Answers[9][3] = "The assassination of Martin Luther King Jr.";

			Answers[10][0] = "Arthur Dent";
			Answers[10][1] = "Paul Atreides";
			Answers[10][2] = "Winston Smith";
			Answers[10][3] = "Rick Deckard";

			Answers[11][0] = "Arthur Dent";
			Answers[11][1] = "Paul Atreides";
			Answers[11][2] = "Winston Smith";
			Answers[11][3] = "Rick Deckard";

			Answers[12][0] = "Arthur C. Clarke";
			Answers[12][1] = "Robert Heinlein";
			Answers[12][2] = "Philip K. Dick";
			Answers[12][3] = "William Gibson";

			Answers[13][0] = "London";
			Answers[13][1] = "Dublin";
			Answers[13][2] = "Paris";
			Answers[13][3] = "New York City";

			Answers[14][0] = "Kvothe";
			Answers[14][1] = "Felurian";
			Answers[14][2] = "Denna";
			Answers[14][3] = "Bast";

			Answers[15][0] = "The Silver Chair";
			Answers[15][1] = "The Lion, the Witch and the Wardrobe";
			Answers[15][2] = "The Voyage of the Dawn Treader";
			Answers[15][3] = "The Magician�s Nephew";

			Answers[16][0] = "Brakebills";
			Answers[16][1] = "Osthorne";
			Answers[16][2] = "Brasenose";
			Answers[16][3] = "Pembroke";

			Answers[17][0] = "A Study in Scarlet";
			Answers[17][1] = "The Sign of the Four";
			Answers[17][2] = "The Valley of Fear";
			Answers[17][3] = "The Hound of the Baskervilles";

			Answers[18][0] = "Dashiell Hammett";
			Answers[18][1] = "James Ellory";
			Answers[18][2] = "James M. Cain";
			Answers[18][3] = "Raymond Chandler";

			Answers[19][0] = "15";
			Answers[19][1] = "14";
			Answers[19][2] = "16";
			Answers[19][3] = "17";

			/*Init Literature Correct Answers*/
			CorrectAnswer[0] = Answers[0][0];
			CorrectAnswer[1] = Answers[1][0];
			CorrectAnswer[2] = Answers[2][0];
			CorrectAnswer[3] = Answers[3][0];
			CorrectAnswer[4] = Answers[4][0];
			CorrectAnswer[5] = Answers[5][0];
			CorrectAnswer[6] = Answers[6][0];
			CorrectAnswer[7] = Answers[7][0];
			CorrectAnswer[8] = Answers[8][0];
			CorrectAnswer[9] = Answers[9][0];
			CorrectAnswer[10] = Answers[10][0];
			CorrectAnswer[11] = Answers[11][0];
			CorrectAnswer[12] = Answers[12][0];
			CorrectAnswer[13] = Answers[13][0];
			CorrectAnswer[14] = Answers[14][0];
			CorrectAnswer[15] = Answers[15][0];
			CorrectAnswer[16] = Answers[16][0];
			CorrectAnswer[17] = Answers[17][0];
			CorrectAnswer[18] = Answers[18][0];
			CorrectAnswer[19] = Answers[19][0];
		}
	}

	//initialise the loaded question playset
	srand(time(NULL));

	playset = rand() % 3 + 1;

	p1turn = 0;
	p1turnoffset = 1;
	p2turn = 0;
	p2turnoffset = 1;
}


int Database::GetQuestions(int player)
{	
	if (player == 1) 
	{
		if (playset == 1) 
		{
			index1 = P1Questions0[p1turn];
			index2 = P1Questions0[p1turnoffset];
			p1turn += 2;
			p1turnoffset += 2;
		}
		else if (playset == 2) 
		{
			index1 = P1Questions1[p1turn];
			index2 = P1Questions1[p1turnoffset];
			p1turn += 2;
			p1turnoffset += 2;
		}
		else 
		{
			index1 = P1Questions2[p1turn];
			index2 = P1Questions2[p1turnoffset];
			p1turn += 2;
			p1turnoffset += 2;
		}
	}
	else 
	{
		if (playset == 1)
		{
			index1 = P2Questions0[p1turn];
			index2 = P2Questions0[p1turnoffset];
			p1turn += 2;
			p1turnoffset += 2;
		}
		else if (playset == 2)
		{
			index1 = P2Questions1[p1turn];
			index2 = P2Questions1[p1turnoffset];
			p1turn += 2;
			p1turnoffset += 2;
		}
		else
		{
			index1 = P2Questions2[p1turn];
			index2 = P2Questions2[p1turnoffset];
			p1turn += 2;
			p1turnoffset += 2;
		}
	}

	return index1, index2;
}

std::string Database::DisplayQuestionChoice()
{
	std::string Q1;
	std::string Q2;

	Q1 = Questions[index1];
	Q2 = Questions[index2];
	
	return Q1, Q2;
}

std::string Database::DisplayQuestion(int index)
{
	std::string Q1;

	Q1 = Questions[index1];

	return Q1;
}

std::string Database::GetAnswers(int index)
{
	Shuffle(index);
	
	std::string A1, A2, A3, A4;

	A1 = Answers[index][0];
	A2 = Answers[index][1];
	A3 = Answers[index][2];
	A4 = Answers[index][3];

	return A1, A2, A3, A4;
}

std::string Database::CheckAnswer(int index, int choice)
{
	std::string answer;


	if (Answers[index][choice] != CorrectAnswer[index])
	{
		answer = "That was the inccorect answer. the right answer is: " + CorrectAnswer[index];
	}
	else 
	{
		answer = "Well Done!";
	}


	return answer;
}
