#pragma once

#include "Library.h"

class Game
{
private:
	//Variables
	std::string GameChoice;
	int GameLevel;
	int turn;

	//chocie variables
	std::string Qchoice1;
	std::string Qchoice2;
	int Qchoice1Index;
	int Qchoice2Index;

	//question variables
	int QIndex;
	std::string A1;
	std::string A2;
	std::string A3;
	std::string A4;
	
	//gameplay variables
	int P1Lives;
	int P2Lives;
	int P1Points;
	int P2Points;

	//Methods
	void PrintRules();
	void GetGameChoice();
	void GetGamelevel();
	void DisplayQuestionChoice();
	void CheckWinner();

public:
	//Variables


	//Methods
	Game();
	virtual ~Game();

	void Play();
};

