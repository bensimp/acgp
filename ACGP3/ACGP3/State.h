#include "TriviaAttack.h"

class State
{
public:
	State();
	virtual ~State();

	TriviaAttack* Game;

	virtual void draw(const float dt) = 0;
	virtual void update(const float dt) = 0;
};
