// Winsock Server Application

#include "stdafx.h"

#include <iostream>
#include <winsock2.h>
#include <vector>
#include <list>
#include <deque>
#include <windows.h>
#include <stdio.h>
#include <process.h>
#include <string>
#include <queue>

#pragma comment(lib, "ws2_32.lib")	// Use this library whilst linking - contains the Winsock2 implementation.

CRITICAL_SECTION CriticalSection;

std::queue<std::string> data_queue;

unsigned __stdcall ReceiveFunction(void *pArguments)
{
	std::cout << "Starting Winsock on thread." << std::endl;

	WSADATA WsaDat;

	// Initialise Windows Sockets
	if (WSAStartup(MAKEWORD(2, 2), &WsaDat) != 0)
	{
		std::cout << "WSA Initialization failed!\r\n";
		WSACleanup();
		return 0;
	}

	// Create a unbound socket.
	SOCKET Socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	if (Socket == INVALID_SOCKET)
	{
		std::cout << "Socket creation failed!";
		WSACleanup();
		return 0;
	}

	// Now, try and bind the socket to any incoming IP address on Port 8888.
	SOCKADDR_IN serverInf;

	serverInf.sin_family = AF_INET;				// Address protocol family - internet protocol (IP addressing).
	serverInf.sin_addr.s_addr = htonl(INADDR_ANY);	// Will accept any IP address from anywhere.
	serverInf.sin_port = htons(8888);			// Port number - can change this, but needs to be the same on both client and server.

	if (bind(Socket, (SOCKADDR*)(&serverInf), sizeof(serverInf)) == SOCKET_ERROR)
	{
		std::cout << "Unable to bind socket!\r\n";
		WSACleanup();
		return 0;
	}

	// Now Sockets have been initialised, so now wait for some data from a client...

	bool finished = false;

	// Repeat 
	while (!finished)
	{
		const int buffer_size = 1024;
		char buffer[buffer_size];	// Space for the data.
		
		struct sockaddr_in client_address;	// Placeholder for client address information - 'recvfrom' will fill this in and return the size of this data in client_address_size.
		int client_address_size = sizeof(client_address);

		std::cout << "Waiting for data from client..." << std::endl;

		int bytes_received = recvfrom(Socket, buffer, buffer_size, 0, (SOCKADDR *)&client_address, &client_address_size);

		if (bytes_received == SOCKET_ERROR)
		{	// If there is an error, deal with it here...
			std::cout << "'recvfrom' failed with error " << WSAGetLastError();
			std::cout << std::endl;
		}
		else
		{
			// No error, so put the data on the queue for the main thread.
			std::string t = buffer;	// t contains the string sent here from the current client.

			if (t == "quit") finished = true;	// Message from the main thread to terminate this thread.

			EnterCriticalSection(&CriticalSection);
			data_queue.push(t);
			LeaveCriticalSection(&CriticalSection);

			// Send something back to the client as an acknowledgement...
			std::string data_to_send = "Message Received and Understood.";
			strcpy(buffer, data_to_send.c_str());
			sendto(Socket, buffer, buffer_size, 0, (SOCKADDR *)&client_address, client_address_size);
		}
	}

	// Shutdown the socket.
	shutdown(Socket, SD_SEND);

	// Close our socket entirely.
	closesocket(Socket);

	// Cleanup Winsock - release any resources used by Winsock.
	WSACleanup();

	_endthreadex(0);
	return 0;
}


int main()
{
	HANDLE hThread;
	unsigned threadID;

	InitializeCriticalSectionAndSpinCount(&CriticalSection, 1000);

	// Create a critical section  object for future use.
	hThread = (HANDLE)_beginthreadex(NULL, 0, &ReceiveFunction, NULL, 0, &threadID);

	// Enter the application's main loop...

	bool done = false;

	while (!done)
	{
		std::string d = "";
		
		EnterCriticalSection(&CriticalSection);	// Access the queue - safely via a critical section.

		if (data_queue.size() > 0)
		{
			d = data_queue.front();	// Retrieve the next data item from the queue.
			data_queue.pop();		// Eliminate it from the queue.
			
			std::cout << "Data Received: " << d;
			std::cout << std::endl;
		}

		LeaveCriticalSection(&CriticalSection);	// Leave the critical section.

		if (d == "quit") done = true;	// Terminate the app.
	}

	// Wait for the receiving thread to complete.
	WaitForSingleObject(hThread, INFINITE);

	// Clean up...
	CloseHandle(hThread);

	return 0;
}
